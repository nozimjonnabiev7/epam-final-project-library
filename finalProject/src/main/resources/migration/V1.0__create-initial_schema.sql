-- Define ENUMs
CREATE TYPE role AS ENUM (
    'ADMIN',
    'LIBRARIAN',
    'READER'
    );

CREATE TYPE place AS ENUM (
    'HALL',
    'HOME'
    );

-- Create user_table
CREATE TABLE IF NOT EXISTS user_table (
                                          user_id         BIGSERIAL       NOT NULL PRIMARY KEY,
                                          first_name      VARCHAR(50)     NOT NULL,
                                          last_name       VARCHAR(50)     NOT NULL,
                                          user_email      VARCHAR(255)    NOT NULL UNIQUE,
                                          user_login      VARCHAR(20)     NOT NULL UNIQUE,
                                          user_password   VARCHAR(255)    NOT NULL,
                                          user_role       role[]          DEFAULT '{READER}', -- Assuming new users are READERS by default
                                          user_blocked    BOOLEAN         DEFAULT false
);

-- Create book_table
CREATE TABLE IF NOT EXISTS book_table (
                                          book_id     BIGSERIAL   NOT NULL PRIMARY KEY,
                                          book_title  VARCHAR(100) NOT NULL,
                                          quantity    INT         NOT NULL
);

-- Create book_order_table
CREATE TABLE IF NOT EXISTS book_order_table (
                                                order_id        BIGSERIAL   NOT NULL PRIMARY KEY,
                                                user_id         BIGINT      NOT NULL,
                                                book_id         BIGINT      NOT NULL,
                                                order_date      DATE        NOT NULL,
                                                returning_date  DATE        NOT NULL,
                                                reading_place   place[]     NOT NULL,
                                                book_returned   BOOLEAN     DEFAULT false,
                                                FOREIGN KEY (book_id) REFERENCES book_table (book_id) ON DELETE CASCADE,
                                                FOREIGN KEY (user_id) REFERENCES user_table (user_id) ON DELETE CASCADE
);
