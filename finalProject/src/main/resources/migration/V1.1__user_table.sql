INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Nozimjon', 'Nabiev', 'nozimjonnabiev7@gmail.com', 'admin', MD5('admin'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Jane', 'Smith', 'janesmith@example.com', 'janesmith', MD5('janesmith'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Michael', 'Johnson', 'michaeljohnson@example.com', 'michaeljohnson', MD5('michaeljohnson'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Emily', 'Brown', 'emilybrown@example.com', 'emilybrown', MD5('emilybrown'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Daniel', 'Lee', 'daniellee@example.com', 'daniellee', MD5('daniellee'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Olivia', 'Wilson', 'oliviawilson@example.com', 'oliviawilson', MD5('oliviawilson'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Ethan', 'Martinez', 'ethanmartinez@example.com', 'ethanmartinez', MD5('ethanmartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Ava', 'Garcia', 'avagarcia@example.com', 'avagarcia', MD5('avagarcia'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('William', 'Jones', 'williamjones@example.com', 'williamjones', MD5('williamjones'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Sophia', 'Rodriguez', 'sophiarodriguez@example.com', 'sophiarodriguez', MD5('sophiarodriguez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('James', 'Lee', 'jameslee@example.com', 'jameslee', MD5('jameslee'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Isabella', 'Gonzalez', 'isabellagonzalez@example.com', 'isabellagonzalez', MD5('isabellagonzalez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Oliver', 'Martinez', 'olivermartinez@example.com', 'olivermartinez', MD5('olivermartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Mia', 'Robinson', 'miarobinson@example.com', 'miarobinson', MD5('miarobinson'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Benjamin', 'Hernandez', 'benjaminhernandez@example.com', 'benjaminhernandez', MD5('benjaminhernandez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Amelia', 'Williams', 'ameliawilliams@example.com', 'ameliawilliams', MD5('ameliawilliams'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Sebastian', 'Lopez', 'sebastianlopez@example.com', 'sebastianlopez', MD5('sebastianlopez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Charlotte', 'Martinez', 'charlottemartinez@example.com', 'charlottemartinez', MD5('charlottemartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Lucas', 'Davis', 'lucasdavis@example.com', 'lucasdavis', MD5('lucasdavis'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Harper', 'Perez', 'harperperez@example.com', 'harperperez', MD5('harperperez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Mason', 'Gonzalez', 'masongonzalez@example.com', 'masongonzalez', MD5('masongonzalez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Evelyn', 'Morales', 'evelynmorales@example.com', 'evelynmorales', MD5('evelynmorales'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Jackson', 'Rodriguez', 'jacksonrodriguez@example.com', 'jacksonrodriguez', MD5('jacksonrodriguez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Aria', 'Lopez', 'arialopez@example.com', 'arialopez', MD5('arialopez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Ella', 'Taylor', 'ellataylor@example.com', 'ellataylor', MD5('ellataylor'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Aiden', 'Martinez', 'aidenmartinez@example.com', 'aidenmartinez', MD5('aidenmartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Scarlett', 'King', 'scarlettking@example.com', 'scarlettking', MD5('scarlettking'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Grace', 'Anderson', 'graceanderson@example.com', 'graceanderson', MD5('graceanderson'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Leo', 'Martinez', 'leomartinez@example.com', 'leomartinez', MD5('leomartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Chloe', 'White', 'chloewhite@example.com', 'chloewhite', MD5('chloewhite'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Mateo', 'Harris', 'mateoharris@example.com', 'mateoharris', MD5('mateoharris'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Luna', 'Lopez', 'lunalopez@example.com', 'lunalopez', MD5('lunalopez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Riley', 'Martinez', 'rileymartinez@example.com', 'rileymartinez', MD5('rileymartinez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Zoey', 'Thomas', 'zoeythomas@example.com', 'zoeythomas', MD5('zoeythomas'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Zachary', 'Walker', 'zacharywalker@example.com', 'zacharywalker', MD5('zacharywalker'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Penelope', 'Perez', 'penelopeperez@example.com', 'penelopeperez', MD5('penelopeperez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Layton', 'Gonzalez', 'laytongonzalez@example.com', 'laytongonzalez', MD5('laytongonzalez'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Nora', 'Ramirez', 'noraramirez@example.com', 'noraramirez', MD5('noraramirez'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Hudson', 'Garcia', 'hudsongarcia@example.com', 'hudsongarcia', MD5('hudsongarcia'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Aubrey', 'Hall', 'aubreyhall@example.com', 'aubreyhall', MD5('aubreyhall'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Isaac', 'Martinez', 'isaalmartinez@example.com', 'isaalmartinez', MD5('isaalmartinez'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Hannah', 'Young', 'hannahyoung@example.com', 'hannahyoung', MD5('hannahyoung'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Stella', 'Flores', 'stellaflores@example.com', 'stellaflores', MD5('stellaflores'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Logan', 'Morales', 'loganmorales@example.com', 'loganmorales', MD5('loganmorales'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Aurora', 'Rivera', 'aurorarivera@example.com', 'aurorarivera', MD5('aurorarivera'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Miles', 'Lopez', 'mileslopez@example.com', 'mileslopez', MD5('mileslopez'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Aaliyah', 'Martinez', 'aaliyahmartinez@example.com', 'aaliyahmartinez', MD5('aaliyahmartinez'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Liam', 'Davis', 'liamdavis@example.com', 'liamdavis', MD5('liamdavis'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Emilia', 'Martinez', 'emiliamartinez@example.com', 'emiliamartinez', MD5('emiliamartinez'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Luke', 'Martinez', 'lukemartinez@example.com', 'lukemartinez', MD5('lukemartinez'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Eleanor', 'Baker', 'eleanorbaker@example.com', 'eleanorbaker', MD5('eleanorbaker'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Carter', 'Garcia', 'cartergarcia@example.com', 'cartergarcia', MD5('cartergarcia'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Hazel', 'Parker', 'hazelparker@example.com', 'hazelparker', MD5('hazelparker'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Gabriel', 'Gonzalez', 'gabrielgonzalez@example.com', 'gabrielgonzalez', MD5('gabrielgonzalez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Mila', 'Wright', 'milawright@example.com', 'milawright', MD5('milawright'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Jack', 'Martinez', 'jackmartinez@example.com', 'jackmartinez', MD5('jackmartinez'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Lily', 'Gomez', 'lilygomez@example.com', 'lilygomez', MD5('lilygomez'), ARRAY['READER']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Caleb', 'Sanchez', 'calebsanchez@example.com', 'calebsanchez', MD5('calebsanchez'), ARRAY['ADMIN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Ellie', 'Anderson', 'ellieanderson@example.com', 'ellieanderson', MD5('ellieanderson'), ARRAY['LIBRARIAN']::role[], false);

INSERT INTO user_table (first_name, last_name, user_email, user_login, user_password, user_role, user_blocked)
VALUES ('Lucas', 'Johnson', 'lucasjohnson@example.com', 'lucasjohnson', MD5('lucasjohnson'), ARRAY['READER']::role[], false);
