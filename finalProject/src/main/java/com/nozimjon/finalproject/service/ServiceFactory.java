package com.nozimjon.finalproject.service;

import com.nozimjon.finalproject.dao.DAOFactory;

import java.sql.Connection;

public class ServiceFactory {

    private DAOFactory daoFactory;

    public ServiceFactory(Connection connection) {
        daoFactory = new DAOFactory(connection);
    }

    public UserService getUserService() {
        return new UserService(daoFactory.getUserDao());
    }

    public BookService getBookService() {
        return new BookService(daoFactory.getBookDao());
    }

    public OrderService getOrderService() {
        return new OrderService(daoFactory.getOrderDao());
    }

}
