package com.nozimjon.finalproject.service;

import com.nozimjon.finalproject.dao.DAOException;
import com.nozimjon.finalproject.dao.OrderDAO;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.dto.UserOrderDisplay;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * The OrderService class provides business logic for order-related operations.
 */
public class OrderService {

    private OrderDAO orderDao;

    /**
     * Constructs an OrderService with the specified OrderDAO.
     *
     * @param orderDao The OrderDAO to be used for database operations.
     */
    public OrderService(OrderDAO orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * Retrieves an order by its ID.
     *
     * @param id The ID of the order to retrieve.
     * @return An Optional containing the Order if found, otherwise an empty Optional.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public Optional<Order> getById(long id) throws ServiceException {
        try {
            return orderDao.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Exception in getById at OrderService class", e);
        }
    }

    /**
     * Retrieves all orders from the database.
     *
     * @return A list of all orders.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Order> getAll() throws ServiceException {
        try {
            return orderDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Exception in getAll at OrderService class", e);
        }
    }

    /**
     * Saves an order to the database.
     *
     * @param order The order to be saved.
     * @throws ServiceException If the order is null or if there is a service-level exception during the operation.
     */
    public void save(Order order) throws ServiceException {
        DataValidator.validateNotNull(order, "Not allow for a null order in save at OrderService class");

        try {
            orderDao.save(order);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServiceException("Exception in save at OrderService class", e);
        }
    }

    /**
     * Removes an order by its ID from the database.
     *
     * @param id The ID of the order to remove.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public void removeById(long id) throws ServiceException {
        try {
            orderDao.removeById(id);
        } catch (DAOException e) {
            throw new ServiceException("Exception in removeById at OrderService class", e);
        }
    }

    /**
     * Updates the details of an order in the database.
     *
     * @param order The updated order.
     * @throws ServiceException If the order is null or if there is a service-level exception during the operation.
     */
    public void update(Order order) throws ServiceException {
        DataValidator.validateNotNull(order, "Not allow for a null order in update at OrderService class");

        try {
            orderDao.update(order);
        } catch (DAOException e) {
            e.printStackTrace();
            throw new ServiceException("Exception in update at OrderService class", e);
        }
    }

    /**
     * Finds orders by the user's ID.
     *
     * @param userId The ID of the user.
     * @return A list of orders associated with the specified user ID.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Order> findOrderByUserId(long userId) throws ServiceException {
        try {
            return orderDao.findOrderByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in findOrderByUserId at OrderService class", e);
        }
    }

    /**
     * Finds an order by the book's ID.
     *
     * @param bookId The ID of the book.
     * @return An Optional containing the Order if found, otherwise an empty Optional.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public Optional<Order> findOrderByBookId(Long bookId) throws ServiceException {
        try {
            return orderDao.findOrderByBookId(bookId);
        } catch (DAOException e) {
            throw new ServiceException("Exception in findOrderByBookId at OrderService class", e);
        }
    }

    /**
     * Retrieves all orders for administration purposes.
     *
     * @return A list of administration order displays.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<AdministrationOrderDisplay> administrationAllOrder() throws ServiceException {
        try {
            return orderDao.administrationAllOrder();
        } catch (DAOException e) {
            throw new ServiceException("Exception in administrationAllOrder at OrderService class", e);
        }
    }

    /**
     * Removes an order by its ID along with increasing the book quantity and committing the transaction.
     *
     * @param orderId            The ID of the order to remove.
     * @param bookId             The ID of the book to increase the quantity.
     * @param bookService        The BookService to update book quantity.
     * @param transactionManager The TransactionManager to handle the transaction.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public void administrationOrderRemoval(String orderId, String bookId, BookService bookService, TransactionManager transactionManager) throws ServiceException {
        try {
            transactionManager.startTransaction();

            increaseBookQuantity(bookId, bookService);
            removeById(Long.valueOf(orderId));
            transactionManager.commitTransaction();
        } catch (SQLException e) {
            transactionManager.rollbackTransaction();
        }
    }

    /**
     * Confirms a user order by saving the order and decreasing the book quantity.
     *
     * @param order              The order to be saved.
     * @param book               The book to decrease the quantity.
     * @param bookService        The BookService to update book quantity.
     * @param transactionManager The TransactionManager to handle the transaction.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public void confirmUserOrder(Order order, Book book, BookService bookService, TransactionManager transactionManager) throws ServiceException {
        try {
            transactionManager.startTransaction();

            save(order);
            decreaseBookQuantity(book, bookService);

            transactionManager.commitTransaction();
        } catch (SQLException e) {
            transactionManager.rollbackTransaction();
        }
    }

    /**
     * Retrieves all user orders for display purposes.
     *
     * @return A list of user order displays.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<UserOrderDisplay> userOrders() throws ServiceException {
        try {
            return orderDao.userOrders();
        } catch (DAOException e) {
            throw new ServiceException("Exception in userOrders at OrderService class", e);
        }
    }

    private void decreaseBookQuantity(Book book, BookService bookService) throws ServiceException {
        Long id = book.getId();
        int quantity = book.getQuantity();
        int updatedQuantity = quantity - 1;
        bookService.updateQuantity(id, updatedQuantity);
    }

    private void increaseBookQuantity(String bookId, BookService bookService) throws ServiceException {
        long id = Long.valueOf(bookId);
        Optional<Book> optionalBook = bookService.getById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            int updatedQuantity = book.getQuantity() + 1;
            bookService.updateQuantity(id, updatedQuantity);
        }
    }
}