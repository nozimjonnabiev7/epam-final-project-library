package com.nozimjon.finalproject.service;

import com.nozimjon.finalproject.dao.DAOException;
import com.nozimjon.finalproject.dao.UserDAO;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.util.validator.entity.UserValidator;

import java.util.List;
import java.util.Optional;

public class UserService implements Service<User>{

    private UserDAO userDao;

    public UserService(UserDAO userDao) {
        this.userDao = userDao;
    }

    public Optional<User> getById(long id) throws ServiceException {
        try {
            return userDao.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in getById method in UserService class", e);
        }
    }

    public Optional<User> findByLoginPassword(String login, String password) throws ServiceException {

        Optional<User> optionalUser = Optional.empty();
        if (UserValidator.isValidLogin(login) && UserValidator.isValidPassword(password)) {
            try {
                optionalUser = userDao.findByLoginAndPassword(login, password);
            } catch (DAOException e) {
                throw new ServiceException("Dao Exception in findByLoginPassword method in UserService class", e);
            }
        }
        return optionalUser;
    }

    public List<User> getAll() throws ServiceException {
        try {
            return userDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in getAll method in UserService class", e);
        }
    }

    public void save(User user) throws ServiceException {
        try {
            userDao.save(user);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in save method in UserService class", e);
        }
    }

    @Override
    public void removeById(long id) throws ServiceException {
        try {
            userDao.removeById(id);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in removeById method in UserService class", e);
        }
    }

    public void update(User user) throws ServiceException {
        try {
            userDao.update(user);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in update method in UserService class", e);
        }
    }

    public Optional<User> findByLogin(String login) throws ServiceException {
        try {
            return userDao.findByLogin(login);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in findByLogin method in UserService class", e);
        }
    }

    public Optional<User> findByEmail(String email) throws ServiceException {
        try {
            return userDao.findByEmail(email);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in findByEmail method in UserService class", e);
        }
    }

    public List<User> findAllWhereRoleReader() throws ServiceException {
        try {
            return userDao.findAllWhereRoleReader();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in findAllWhereRoleReader method in UserService class", e);
        }
    }

    public List<User> sortUsersByName() throws ServiceException {
        try {
            return userDao.sortUsersByName();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in sortUsersByName method in UserService class", e);
        }
    }

    public List<User> sortUsersByEmail() throws ServiceException {
        try {
            return userDao.sortUsersByEmail();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in sortUsersByEmail method in UserService class", e);
        }
    }

    public void updateRole(Long id, String role) throws ServiceException {
        try {
            userDao.updateRole(id, role);
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in updateRole method in UserService class", e);
        }
    }

    public List<User> adminSortUsersByName() throws ServiceException {
        try {
            return userDao.adminSortUsersByName();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in adminSortUsersByName method in UserService class", e);
        }
    }

    public List<User> adminSortUsersByEmail() throws ServiceException {
        try {
            return userDao.adminSortUsersByEmail();
        } catch (DAOException e) {
            throw new ServiceException("Dao Exception in adminSortUsersByEmail method in UserService class", e);
        }
    }
}
