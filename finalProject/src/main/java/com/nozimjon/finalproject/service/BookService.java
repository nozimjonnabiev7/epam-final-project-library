package com.nozimjon.finalproject.service;

import com.nozimjon.finalproject.dao.BookDAO;
import com.nozimjon.finalproject.dao.DAOException;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.util.List;
import java.util.Optional;

/**
 * The BookService class provides business logic for book-related operations.
 */
public class BookService {

    private BookDAO bookDao;

    /**
     * Constructs a BookService with the specified BookDAO.
     *
     * @param bookDao The BookDAO to be used for database operations.
     */
    public BookService(BookDAO bookDao) {
        this.bookDao = bookDao;
    }

    /**
     * Retrieves a book by its ID.
     *
     * @param id The ID of the book to retrieve.
     * @return An Optional containing the Book if found, otherwise an empty Optional.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public Optional<Book> getById(long id) throws ServiceException {
        try {
            return bookDao.getById(id);
        } catch (DAOException e) {
            throw new ServiceException("Exception in getById at BookService class", e);
        }
    }

    /**
     * Retrieves all books from the database.
     *
     * @return A list of all books.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Book> getAll() throws ServiceException {
        try {
            return bookDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException("Exception in findAllBook at BookService class", e);
        }
    }

    /**
     * Saves a book to the database.
     *
     * @param book The book to be saved.
     * @throws ServiceException If the book is null or if there is a service-level exception during the operation.
     */
    public void save(Book book) throws ServiceException {
        DataValidator.validateNotNull(book, "Not allow for a null book in save at BookService class");
        try {
            bookDao.save(book);
        } catch (DAOException e) {
            throw new ServiceException("Exception in save at BookService class", e);
        }
    }

    /**
     * Removes a book by its ID from the database.
     *
     * @param id The ID of the book to remove.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public void removeById(long id) throws ServiceException {
        try {
            bookDao.removeById(id);
        } catch (DAOException e) {
            throw new ServiceException("Exception in removeById at BookService class", e);
        }
    }

    /**
     * Updates the details of a book in the database.
     *
     * @param book The updated book.
     * @throws ServiceException If the book is null or if there is a service-level exception during the operation.
     */
    public void update(Book book) throws ServiceException {
        DataValidator.validateNotNull(book, "Not allow for a null book in update at BookService class");
        try {
            bookDao.update(book);
        } catch (DAOException e) {
            throw new ServiceException("Exception in update at BookService class", e);
        }
    }

    /**
     * Updates the quantity of a book in the database.
     *
     * @param bookId   The ID of the book to update.
     * @param quantity The new quantity value.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public void updateQuantity(Long bookId, int quantity) throws ServiceException {
        try {
            bookDao.updateQuantity(bookId, quantity);
        } catch (DAOException e) {
            throw new ServiceException("Exception in updateQuantity at BookService class", e);
        }
    }

    /**
     * Finds books by their name.
     *
     * @param name The name of the book to find.
     * @return A list of books with the specified name.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Book> findByName(String name) throws ServiceException {
        try {
            return bookDao.findByName(name);
        } catch (DAOException e) {
            throw new ServiceException("Exception in findByName at BookService class", e);
        }
    }

    // Sorting

    /**
     * Retrieves all books from the database and sorts them by name.
     *
     * @return A list of books sorted by name.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Book> sortBooksByName() throws ServiceException {
        try {
            return bookDao.sortBooksByName();
        } catch (DAOException e) {
            throw new ServiceException("Exception in sortBooksByName at BookService class", e);
        }
    }

    /**
     * Retrieves all books from the database and sorts them by quantity.
     *
     * @return A list of books sorted by quantity.
     * @throws ServiceException If there is a service-level exception during the operation.
     */
    public List<Book> sortBookByQuantity() throws ServiceException {
        try {
            return bookDao.sortBooksByQuantity();
        } catch (DAOException e) {
            throw new ServiceException("Exception in sortBookByQuantity at BookService class", e);
        }
    }
}
