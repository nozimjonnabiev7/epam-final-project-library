package com.nozimjon.finalproject.service;

import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManager {

    public final static boolean DISABLE_AUTO_COMMIT = false;
    private Connection connection;

    public TransactionManager(Connection connection) {
        this.connection = connection;
    }

    public void startTransaction() throws SQLException {
        connection.setAutoCommit(DISABLE_AUTO_COMMIT);
    }

    public void commitTransaction() throws SQLException {
        connection.commit();
    }

    public void rollbackTransaction() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
