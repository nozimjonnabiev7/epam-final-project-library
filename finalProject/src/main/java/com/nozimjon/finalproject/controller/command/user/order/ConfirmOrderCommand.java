package com.nozimjon.finalproject.controller.command.user.order;

import com.nozimjon.finalproject.controller.builder.OrderBuilderFromRequest;
import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.TransactionManager;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;
import com.nozimjon.finalproject.util.validator.entity.OrderValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * A command to confirm a user's book order.
 */
public class ConfirmOrderCommand implements Command {

    private final BookService bookService;
    private final OrderService orderService;
    private final TransactionManager transactionManager;
    private final OrderBuilderFromRequest builderFromRequest = new OrderBuilderFromRequest();

    public ConfirmOrderCommand(final BookService bookService,
                               final OrderService orderService, final TransactionManager transactionManager) {

        this.bookService = bookService;
        this.orderService = orderService;
        this.transactionManager = transactionManager;
    }

    @Override
    public CommandResult execute(final HttpServletRequest request,
                                 final HttpServletResponse response) throws ServiceException {

        CommandResult commandResult = new CommandResult();
        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);

        if (user != null) {
            List<String> orderValidation = validateOrder(request);
            if (orderValidation.isEmpty()) {
                commandResult = confirmOrder(request, user.getId());
            } else {
                request.setAttribute(Operation.VALIDATION_LIST, orderValidation);
                commandResult.forward(request.getContextPath() + PageLocation.VIEW_BOOK);
            }
        }
        return commandResult;
    }

    /**
     * Validates the order parameters.
     *
     * @param request The HTTP servlet request containing the order parameters.
     * @return A list of validation messages.
     */
    private List<String> validateOrder(HttpServletRequest request) {
        return OrderValidator.validateOrderParameter(request);
    }

    /**
     * Confirms the user's book order.
     *
     * @param request The HTTP servlet request containing the order details.
     * @param userId  The ID of the user placing the order.
     * @return The CommandResult with the redirect page after confirming the order.
     * @throws ServiceException If there is an error while confirming the order.
     */
    private CommandResult confirmOrder(HttpServletRequest request, Long userId) throws ServiceException {
        String operation;
        CommandResult commandResult = new CommandResult();
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        Optional<Book> optionalBook = bookService.getById(Long.parseLong(bookId));

        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            Order order = builderFromRequest.userOrder(request, book.getId(), userId);
            orderService.confirmUserOrder(order, book, bookService, transactionManager);
            operation = Operation.ORDER_CONFIRMED;
            commandResult.redirect(request.getContextPath() + RedirectTo.BOOK_LIST_PAGE +
                    Operation.OPERATION_STATUS + operation);
        } else {
            request.setAttribute(Operation.CONFIRM_FAIL, DiffConstant.READ_FROM_PROPERTIES);
            commandResult.forward(request.getContextPath() + PageLocation.VIEW_BOOK);
        }
        return commandResult;
    }
}
