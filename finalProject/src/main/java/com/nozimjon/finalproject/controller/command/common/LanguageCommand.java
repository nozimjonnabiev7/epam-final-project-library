package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * A command to change the language setting of the application.
 */
public class LanguageCommand implements Command {

    private static final String LANGUAGE_PARAMETER = "language";
    private static final String LANGUAGE_ATTRIBUTE = "language";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession();
        String language = request.getParameter(LANGUAGE_PARAMETER);

        if (isValidLanguage(language)) {
            session.setAttribute(LANGUAGE_ATTRIBUTE, language);
        } else {
            // Default to English if the language parameter is missing or invalid
            session.setAttribute(LANGUAGE_ATTRIBUTE, "en");
        }

        return new CommandResult(PageLocation.MAIN_PAGE);
    }

    /**
     * Checks if the provided language is valid.
     *
     * @param language The language string to check.
     * @return true if the language is valid, false otherwise.
     */
    private boolean isValidLanguage(String language) {
        return language != null && !language.isEmpty();
    }
}
