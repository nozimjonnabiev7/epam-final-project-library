package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Command to search for orders in the administration panel based on different criteria.
 */
public class AdministrationSearchOrderCommand implements Command {

    private final static String SEARCH_VALUE = "query";
    private final static String SEARCH_CRITERIA = "type";
    private final static String BOOK_NAME = "bookName";
    private final static String USER_NAME = "userName";
    private final static String USER_EMAIL = "email";
    private static final String ORDER_DATE = "orderDate";
    private final static String RETURNING_DATE = "returningDate";
    private final static String READING_PLACE = "readingPlace";

    private final OrderService orderService;

    /**
     * Constructs an AdministrationSearchOrderCommand with the specified OrderService.
     *
     * @param orderService The OrderService used to handle order-related operations.
     */
    public AdministrationSearchOrderCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Executes the command to search for orders in the administration panel based on different criteria and displays the search results.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String searchValue = request.getParameter(SEARCH_VALUE);
        String searchCriteria = request.getParameter(SEARCH_CRITERIA);

        List<AdministrationOrderDisplay> orders = orderService.administrationAllOrder();
        List<AdministrationOrderDisplay> searchResult = findOrder(searchCriteria, searchValue, orders);
        if (searchResult != null) {
            request.setAttribute(OrderConstant.ORDER_LIST, searchResult);
        } else {
            request.setAttribute(OrderConstant.ORDER_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
        }

        return new CommandResult(PageLocation.ADMINISTRATION_ORDER_LIST);
    }

    /**
     * Find orders based on the specified search criteria and query.
     *
     * @param criteria        The search criteria, such as book name, username, etc.
     * @param query           The search query to find matching orders.
     * @param orderDisplayList The list of all orders to be searched.
     * @return The list of AdministrationOrderDisplay containing the search results.
     */
    private List<AdministrationOrderDisplay> findOrder(String criteria, String query, List<AdministrationOrderDisplay> orderDisplayList) {
        List<AdministrationOrderDisplay> result;
        switch (criteria) {
            case BOOK_NAME:
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getBookName().toLowerCase().contains(query.toLowerCase()))
                        .collect(Collectors.toList());
                break;
            case USER_NAME:
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getUserName().equalsIgnoreCase(query))
                        .collect(Collectors.toList());
                break;
            case USER_EMAIL:
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getUserEmail().toLowerCase().contains(query.toLowerCase()))
                        .collect(Collectors.toList());
                break;
            case ORDER_DATE:
                Date orderDate = Date.valueOf(query);
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getOrderDate().equals(orderDate))
                        .collect(Collectors.toList());
                break;
            case RETURNING_DATE:
                Date returningDate = Date.valueOf(query);
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getReturningDate().equals(returningDate))
                        .collect(Collectors.toList());
                break;
            case READING_PLACE:
                ReadingPlace readingPlace = EnumService.getReadingPlace(query);
                result = orderDisplayList.stream()
                        .filter(administrationOrderDisplay -> administrationOrderDisplay.getReadingPlace().equals(readingPlace))
                        .collect(Collectors.toList());
                break;
            default:
                result = orderDisplayList;
        }
        return result;
    }
}
