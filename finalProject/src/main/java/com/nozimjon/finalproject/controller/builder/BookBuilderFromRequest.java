package com.nozimjon.finalproject.controller.builder;

import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;

import javax.servlet.http.HttpServletRequest;

/**
 * Builder class to construct Book objects from HttpServletRequest.
 */
public class BookBuilderFromRequest {

    /**
     * Builds a Book object for updating based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the book information.
     * @return The Book object with the updated data.
     */
    public Book buildBookToUpdate(HttpServletRequest request) {
        String id = request.getParameter(BookConstant.BOOK_ID);
        String name = request.getParameter(BookConstant.BOOK_NAME);
        String quantity = request.getParameter(BookConstant.BOOK_QUANTITY);

        return createBookWithIdAndQuantity(id, name, quantity);
    }

    /**
     * Builds a Book object for adding a new book based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the new book information.
     * @return The Book object with the data for the new book.
     */
    public Book buildBookToAdd(HttpServletRequest request) {
        String name = request.getParameter(BookConstant.BOOK_NAME);
        String quantity = request.getParameter(BookConstant.BOOK_QUANTITY);

        return createBookWithQuantity(name, quantity);
    }

    /**
     * Creates a Book object with the specified id and quantity.
     *
     * @param id       The book ID.
     * @param name     The book name.
     * @param quantity The book quantity.
     * @return The Book object with the specified id and quantity.
     */
    private Book createBookWithIdAndQuantity(String id, String name, String quantity) {
        long bookId = Long.parseLong(id);
        int bookQuantity = Integer.parseInt(quantity);
        return new Book(bookId, name, bookQuantity);
    }

    /**
     * Creates a Book object with the specified name and quantity.
     *
     * @param name     The book name.
     * @param quantity The book quantity.
     * @return The Book object with the specified name and quantity.
     */
    private Book createBookWithQuantity(String name, String quantity) {
        int bookQuantity = Integer.parseInt(quantity);
        return new Book(name, bookQuantity);
    }
}
