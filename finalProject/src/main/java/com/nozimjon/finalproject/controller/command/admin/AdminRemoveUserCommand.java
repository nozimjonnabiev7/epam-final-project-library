package com.nozimjon.finalproject.controller.command.admin;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle removing a user by the administrator.
 */
public class AdminRemoveUserCommand implements Command {

    private static final Logger logger = LogManager.getLogger();
    private final UserService userService;

    /**
     * Constructs an AdminRemoveUserCommand with the specified UserService.
     *
     * @param userService The UserService used to handle user-related operations.
     */
    public AdminRemoveUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String operation;
        CommandResult commandResult = new CommandResult();
        String userId = request.getParameter(UserConstant.ID);
        List<User> users = userService.getAll();
        try {
            userService.removeById(Long.parseLong(userId));
            operation = Operation.REMOVED;
        } catch (ServiceException e) {
            operation = Operation.REMOVE_FAIL;
            logger.error("Error while removing user", e);
        }

        request.setAttribute(UserConstant.USER_LIST, users);
        commandResult.redirect(RedirectTo.ADMINISTRATION_USER_LIST_PAGE + Operation.OPERATION_STATUS + operation);
        return commandResult;
    }

}
