package com.nozimjon.finalproject.controller.command.administration.user;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle displaying the list of users in the administration section.
 */
public class AdministrationUserListCommand implements Command {

    private final UserService userService;

    /**
     * Constructs an AdministrationUserListCommand with the specified UserService.
     *
     * @param userService The UserService used to handle user-related operations.
     */
    public AdministrationUserListCommand(UserService userService) {
        this.userService = userService;
    }

    /**
     * Executes the command to retrieve and display the list of users.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);

        List<User> users = null;
        if (user.getRole().equals(Roles.ADMIN)) {
            users = userService.getAll();
        } else if (user.getRole().equals(Roles.LIBRARIAN)) {
            users = userService.findAllWhereRoleReader();
        }
        request.setAttribute(UserConstant.USER_LIST, users);

        return new CommandResult(PageLocation.ADMINISTRATION_USER_LIST);
    }
}
