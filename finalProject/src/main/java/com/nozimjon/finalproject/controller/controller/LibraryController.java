package com.nozimjon.finalproject.controller.controller;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandFactory;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.db.ConnectionPool;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The LibraryController is a servlet-based controller that handles requests and dispatches them to appropriate commands.
 */
@WebServlet("/controller")
public class LibraryController extends HttpServlet {
    private static final String COMMAND_NAME = "command";
    private static final Logger logger = LogManager.getLogger();

    /**
     * Handles HTTP GET requests.
     *
     * @param request  the HttpServletRequest object
     * @param response the HttpServletResponse object
     * @throws ServletException if the request could not be handled
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles HTTP POST requests.
     *
     * @param request  the HttpServletRequest object
     * @param response the HttpServletResponse object
     * @throws ServletException if the request could not be handled
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processes the incoming request and delegates it to the appropriate command for execution.
     *
     * @param request  the HttpServletRequest object
     * @param response the HttpServletResponse object
     * @throws ServletException if the request could not be handled
     * @throws IOException      if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter(COMMAND_NAME);

        try (CommandFactory factory = new CommandFactory()) {
            Command action = factory.create(command);
            CommandResult commandResult = action.execute(request, response);
            dispatch(request, response, commandResult);
        } catch (ServiceException e) {
            logger.error("Exception in Library Controller", e);
            response.sendRedirect(PageLocation.ERROR_PAGE);
        }
    }

    /**
     * Dispatches the request to the appropriate view or URL based on the command result.
     *
     * @param request       the HttpServletRequest object
     * @param response      the HttpServletResponse object
     * @param commandResult the CommandResult object containing the result of the command execution
     * @throws ServletException if the request could not be handled
     * @throws IOException      if an I/O error occurs
     */
    private void dispatch(HttpServletRequest request, HttpServletResponse response, CommandResult commandResult) throws ServletException, IOException {
        String page = commandResult.getPage();
        switch (commandResult.getCommandAction()) {
            case FORWARD:
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
                break;
            case REDIRECT:
                response.sendRedirect(request.getContextPath() + page);
                break;
            default:
                response.sendRedirect(request.getContextPath() + RedirectTo.LOGIN_PAGE);
        }
    }

    /**
     * Cleans up resources when the servlet is being destroyed.
     */
    @Override
    public void destroy() {
        ConnectionPool.getInstance().closePool();
    }
}
