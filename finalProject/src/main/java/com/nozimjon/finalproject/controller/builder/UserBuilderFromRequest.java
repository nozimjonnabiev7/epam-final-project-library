package com.nozimjon.finalproject.controller.builder;

import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;

/**
 * Builder class to construct User objects from HttpServletRequest.
 */
public class UserBuilderFromRequest {

    /**
     * Builds a User object for updating based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the user information.
     * @return The User object with the updated data.
     * @throws NumberFormatException if ID or blocked field parsing fails.
     */
    public User buildUserForUpdate(HttpServletRequest request) throws NumberFormatException {
        long id = Long.parseLong(request.getParameter(UserConstant.ID));
        String name = request.getParameter(UserConstant.NAME);
        String lastName = request.getParameter(UserConstant.LAST_NAME);
        String email = request.getParameter(UserConstant.EMAIL);
        String login = request.getParameter(UserConstant.LOGIN);
        Roles role = EnumService.getRole(request.getParameter(UserConstant.ROLE));
        boolean blocked = Boolean.parseBoolean(request.getParameter(UserConstant.BLOCKED).trim());

        return new User(id, name, lastName, email, login, role, blocked);
    }

    /**
     * Builds a User object for inserting a new user based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the new user information.
     * @return The User object with the data for the new user.
     */
    public User buildUserForInserting(HttpServletRequest request) {
        String name = request.getParameter(UserConstant.NAME);
        String lastName = request.getParameter(UserConstant.LAST_NAME);
        String email = request.getParameter(UserConstant.EMAIL);
        String login = request.getParameter(UserConstant.LOGIN);
        String password = request.getParameter(UserConstant.PASSWORD);
        return new User(name, lastName, email, login, password);
    }
}
