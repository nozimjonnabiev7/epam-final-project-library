package com.nozimjon.finalproject.controller.command.administration.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.nozimjon.finalproject.controller.builder.UserBuilderFromRequest;
import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;
import com.nozimjon.finalproject.util.validator.entity.UserValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle updating user information in the administration section.
 */
public class AdministrationUpdateUserCommand implements Command {

    private static final Logger logger = LogManager.getLogger();

    private final UserService userService;
    private final UserBuilderFromRequest builderFromRequest = new UserBuilderFromRequest();

    /**
     * Constructs an AdministrationUpdateUserCommand with the specified UserService.
     *
     * @param userService The UserService used to handle user-related operations.
     */
    public AdministrationUpdateUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {

        CommandResult commandResult;
        String userId = request.getParameter(UserConstant.ID);

        if (userId != null && !userId.isEmpty()) {
            commandResult = updateUser(request);
        } else {
            commandResult = insertUser(request);
        }
        return commandResult;
    }

    /**
     * Updates an existing user with the information provided in the request.
     *
     * @param request The HTTP request containing the user information to update.
     * @return The CommandResult that will be used for redirection or forwarding.
     */
    private CommandResult updateUser(HttpServletRequest request) {
        String operation = null;
        CommandResult commandResult = new CommandResult();
        List<String> userValidation = UserValidator.validateUserParameter(request);
        if (userValidation.isEmpty()) {
            User updateUser = builderFromRequest.buildUserForUpdate(request);
            try {
                userService.update(updateUser);
                operation = Operation.UPDATED;
            } catch (ServiceException e) {
                operation = Operation.UPDATE_FAIL;
                logger.error(e);
            } finally {
                commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_USER_PAGE + Operation.OPERATION_STATUS + operation);
            }
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, userValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_USER);
        }
        return commandResult;
    }

    /**
     * Inserts a new user with the information provided in the request.
     *
     * @param request The HTTP request containing the user information to insert.
     * @return The CommandResult that will be used for redirection or forwarding.
     */
    private CommandResult insertUser(HttpServletRequest request) {
        String operation = null;
        CommandResult commandResult = new CommandResult();
        List<String> userValidation = UserValidator.validateUserParameter(request);
        if (userValidation.isEmpty()) {
            User user = builderFromRequest.buildUserForInserting(request);
            try {
                userService.save(user);
                operation = Operation.INSERTED;
            } catch (ServiceException e) {
                operation = Operation.INSERT_FAIL;
                logger.error(e);
            } finally {
                commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_USER_PAGE + Operation.OPERATION_STATUS + operation);
            }
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, userValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_USER);
        }
        return commandResult;
    }
}