package com.nozimjon.finalproject.controller.command.administration.order;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

/**
 * Command to handle editing an order in the administration section.
 */
public class AdministrationEditOrderCommand implements Command {
    private final OrderService orderService;
    private final BookService bookService;
    private final UserService userService;

    /**
     * Constructs an AdministrationEditOrderCommand with the specified OrderService, BookService, and UserService.
     *
     * @param orderService The OrderService used to handle order-related operations.
     * @param bookService  The BookService used to handle book-related operations.
     * @param userService The UserService used to handle user-related operations.
     */
    public AdministrationEditOrderCommand(OrderService orderService, BookService bookService, UserService userService) {
        this.orderService = orderService;
        this.bookService = bookService;
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String page;
        String orderId = request.getParameter(OrderConstant.ORDER_ID);

        List<Book> bookList = bookService.getAll();
        List<User> userList = userService.getAll();
        if (orderId != null) {
            Optional<Order> optionalOrder = orderService.getById(Long.parseLong(orderId));
            if (optionalOrder.isPresent()) {
                Order order = optionalOrder.get();
                request.setAttribute(OrderConstant.EDIT_ORDER, order);
                request.setAttribute(BookConstant.BOOK_LIST, bookList);
                request.setAttribute(UserConstant.USER_LIST, userList);
                page = PageLocation.ADMINISTRATION_EDIT_ORDER;
            } else {
                request.setAttribute(OrderConstant.ORDER_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
                page = PageLocation.ADMINISTRATION_ORDER_LIST;
            }
        } else {
            request.setAttribute(BookConstant.BOOK_LIST, bookList);
            request.setAttribute(UserConstant.USER_LIST, userList);
            page = PageLocation.ADMINISTRATION_EDIT_ORDER;
        }

        return new CommandResult(page);
    }
}
