package com.nozimjon.finalproject.controller.command.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * Command to handle searching for books and displaying search results.
 */
public class SearchBookCommand implements Command {

    private final static String SEARCH_VALUE = "query";
    private final static String SEARCH_CRITERIA = "type";

    private final BookService bookService;

    /**
     * Constructs a SearchBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public SearchBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * Executes the command to search for books based on search criteria and value, and displays the search results.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);

        String searchValue = request.getParameter(SEARCH_VALUE);
        String searchCriteria = request.getParameter(SEARCH_CRITERIA);

        List<Book> bookList = findBook(searchCriteria, searchValue);

        if (Objects.requireNonNull(bookList).size() > 0) {
            request.setAttribute(BookConstant.BOOK_LIST, bookList);
        } else {
            request.setAttribute(BookConstant.BOOK_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
        }

        return commandResult(user);
    }

    private List<Book> findBook(String searchCriteria, String searchValue) throws ServiceException {
        List<Book> bookList = null;
        if (searchCriteria.equalsIgnoreCase(BookConstant.BOOK_NAME)) {
            bookList = bookService.findByName(searchValue);
        }
        return bookList;
    }

    private CommandResult commandResult(User user) {
        String page;
        if (user == null) {
            page = PageLocation.BOOK_STORE;
        } else if (user.getRole().equals(Roles.ADMIN) || user.getRole().equals(Roles.LIBRARIAN)) {
            page = PageLocation.ADMINISTRATION_BOOK_STORE;
        } else {
            page = PageLocation.BOOK_STORE;
        }
        return new CommandResult(page);
    }
}
