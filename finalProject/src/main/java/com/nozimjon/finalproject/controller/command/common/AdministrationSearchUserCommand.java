package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A command to search for users in the administration panel based on different criteria.
 */
public class AdministrationSearchUserCommand implements Command {

    private static final String SEARCH_VALUE = "query";
    private static final String SEARCH_CRITERIA = "type";

    private final UserService userService;

    /**
     * Constructs the AdministrationSearchUserCommand.
     *
     * @param userService The UserService to use for fetching users.
     */
    public AdministrationSearchUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String searchValue = request.getParameter(SEARCH_VALUE);
        String searchCriteria = request.getParameter(SEARCH_CRITERIA);

        Optional<User> optionalUser = findUser(searchCriteria, searchValue);

        List<User> userList = new ArrayList<>();
        if (optionalUser.isPresent()) {
            userList.add(optionalUser.get());
            request.setAttribute(UserConstant.USER_LIST, userList);
        } else {
            request.setAttribute(UserConstant.USER_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
        }
        return new CommandResult(PageLocation.ADMINISTRATION_USER_LIST);
    }

    /**
     * Finds the user that matches the specified criteria and query.
     *
     * @param searchCriteria The search criteria for filtering users.
     * @param searchValue    The search query to match against the specified criteria.
     * @return An optional User that matches the search criteria and query, or empty if not found.
     * @throws ServiceException If there is an error while fetching the user from the UserService.
     */
    private Optional<User> findUser(String searchCriteria, String searchValue) throws ServiceException {
        Optional<User> optionalUser = Optional.empty();
        if (UserConstant.LOGIN.equalsIgnoreCase(searchCriteria)) {
            optionalUser = userService.findByLogin(searchValue);
        } else if (UserConstant.EMAIL.equalsIgnoreCase(searchCriteria)) {
            optionalUser = userService.findByEmail(searchValue);
        }
        return optionalUser;
    }
}
