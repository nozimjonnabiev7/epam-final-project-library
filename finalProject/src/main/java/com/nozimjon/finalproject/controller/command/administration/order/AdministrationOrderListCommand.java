package com.nozimjon.finalproject.controller.command.administration.order;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle listing all orders in the administration section.
 */
public class AdministrationOrderListCommand implements Command {
    private final OrderService orderService;

    /**
     * Constructs an AdministrationOrderListCommand with the specified OrderService.
     *
     * @param orderService The OrderService used to handle order-related operations.
     */
    public AdministrationOrderListCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        List<AdministrationOrderDisplay> orders = orderService.administrationAllOrder();
        request.setAttribute(OrderConstant.ORDER_LIST, orders);
        return new CommandResult(PageLocation.ADMINISTRATION_ORDER_LIST);
    }
}
