package com.nozimjon.finalproject.controller.command.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Command to view the details of a book by its ID.
 */
public class ViewBookCommand implements Command {

    private final BookService bookService;

    /**
     * Constructs a ViewBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public ViewBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * Executes the command to view the details of a book by its ID and displays the book details.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String page;
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        Optional<Book> optionalBook = bookService.getById(Long.parseLong(bookId));
        if (optionalBook.isPresent()) {
            request.setAttribute(BookConstant.BOOK_ATTRIBUTE, optionalBook.get());
            page = PageLocation.VIEW_BOOK;
        } else {
            request.setAttribute(BookConstant.BOOK_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
            page = PageLocation.BOOK_STORE;
        }
        return new CommandResult(page);
    }
}
