package com.nozimjon.finalproject.controller.command;

import com.nozimjon.finalproject.controller.command.admin.AdminRemoveBookCommand;
import com.nozimjon.finalproject.controller.command.admin.AdminRemoveUserCommand;
import com.nozimjon.finalproject.controller.command.administration.book.AdministrationAddBookCommand;
import com.nozimjon.finalproject.controller.command.administration.book.AdministrationEditBookCommand;
import com.nozimjon.finalproject.controller.command.administration.book.AdministrationUpdateBookCommand;
import com.nozimjon.finalproject.controller.command.administration.order.AdministrationEditOrderCommand;
import com.nozimjon.finalproject.controller.command.administration.order.AdministrationOrderListCommand;
import com.nozimjon.finalproject.controller.command.administration.order.AdministrationUpdateOrderCommand;
import com.nozimjon.finalproject.controller.command.administration.user.AdministrationEditUserCommand;
import com.nozimjon.finalproject.controller.command.administration.user.AdministrationUpdateUserCommand;
import com.nozimjon.finalproject.controller.command.administration.user.AdministrationUserListCommand;
import com.nozimjon.finalproject.controller.command.book.AddBookCommand;
import com.nozimjon.finalproject.controller.command.book.SearchBookCommand;
import com.nozimjon.finalproject.controller.command.book.SortBookCommand;
import com.nozimjon.finalproject.controller.command.book.ViewBookCommand;
import com.nozimjon.finalproject.controller.command.common.*;
import com.nozimjon.finalproject.controller.command.user.UserOrderCommand;
import com.nozimjon.finalproject.controller.command.user.order.ConfirmOrderCommand;
import com.nozimjon.finalproject.controller.command.user.order.OrderBookCommand;
import com.nozimjon.finalproject.db.ConnectionPool;
import com.nozimjon.finalproject.service.ServiceFactory;
import com.nozimjon.finalproject.service.TransactionManager;
import com.nozimjon.finalproject.util.constant.CommandName;

import java.sql.Connection;

/**
 * The CommandFactory class is responsible for creating specific command objects based on the given command name.
 */
public class CommandFactory implements AutoCloseable {

    private ServiceFactory serviceFactory;
    private TransactionManager transactionManager;
    private Connection connection;

    public CommandFactory() {
        connection = ConnectionPool.getInstance().getConnection();
        serviceFactory = new ServiceFactory(connection);
        transactionManager = new TransactionManager(connection);
    }

    /**
     * Creates and returns a specific command object based on the given command name.
     *
     * @param command the command name representing the type of command to create.
     * @return the specific command object.
     */
    public Command create(final String command) {
        switch (command) {
            case CommandName.REGISTRATION:
                return new SignUpCommand(serviceFactory.getUserService());
            case CommandName.LOGIN:
                return new LoginCommand(serviceFactory.getUserService());
            case CommandName.LOGOUT:
                return new LogOutCommand();
            case CommandName.CHANGE_LANGUAGE:
                return new LanguageCommand();
            case CommandName.PROFILE:
                return new ProfileCommand();

            // ADMINISTRATION

            case CommandName.ADMINISTRATION_BOOK_STORE:
                return new AdministrationAddBookCommand(serviceFactory.getBookService());
            case CommandName.ADMINISTRATION_EDIT_BOOK:
                return new AdministrationEditBookCommand(serviceFactory.getBookService());
            case CommandName.ADMINISTRATION_UPDATE_BOOK:
                return new AdministrationUpdateBookCommand(serviceFactory.getBookService());

            case CommandName.ADMINISTRATION_ORDER_LIST:
                return new AdministrationOrderListCommand(serviceFactory.getOrderService());
            case CommandName.ADMINISTRATION_EDIT_ORDER:
                return new AdministrationEditOrderCommand(serviceFactory.getOrderService(), serviceFactory.getBookService(),
                        serviceFactory.getUserService());
            case CommandName.ADMINISTRATION_UPDATE_ORDER:
                return new AdministrationUpdateOrderCommand(serviceFactory.getOrderService(),
                        serviceFactory.getBookService(), transactionManager);
            case CommandName.ADMINISTRATION_SORT_ORDER:
                return new AdministrationSortOrderCommand(serviceFactory.getOrderService());
            case CommandName.ADMINISTRATION_SEARCH_ORDER:
                return new AdministrationSearchOrderCommand(serviceFactory.getOrderService());

            case CommandName.ADMINISTRATION_DISPLAY_USER:
                return new AdministrationUserListCommand(serviceFactory.getUserService());
            case CommandName.ADMINISTRATION_EDIT_USER:
                return new AdministrationEditUserCommand(serviceFactory.getUserService());
            case CommandName.ADMINISTRATION_UPDATE_USER:
                return new AdministrationUpdateUserCommand(serviceFactory.getUserService());
            case CommandName.ADMINISTRATION_SEARCH_USER:
                return new AdministrationSearchUserCommand(serviceFactory.getUserService());
            case CommandName.ADMINISTRATION_SORT_USER:
                return new AdministrationSortUserCommand(serviceFactory.getUserService());

            // Admin only
            case CommandName.ADMIN_REMOVE_BOOK:
                return new AdminRemoveBookCommand(serviceFactory.getBookService());
            case CommandName.ADMIN_REMOVE_USER:
                return new AdminRemoveUserCommand(serviceFactory.getUserService());

            // User
            case CommandName.DISPLAY_BOOK:
                return new AddBookCommand(serviceFactory.getBookService());
            case CommandName.CONFIRM_ORDER:
                return new ConfirmOrderCommand(serviceFactory.getBookService(), serviceFactory.getOrderService(),
                        transactionManager);
            case CommandName.USER_ORDER:
                return new UserOrderCommand(serviceFactory.getOrderService());

            // book
            case CommandName.VIEW_BOOK:
                return new ViewBookCommand(serviceFactory.getBookService());
            case CommandName.ORDER_BOOK:
                return new OrderBookCommand(serviceFactory.getBookService());
            case CommandName.SEARCH_BOOK:
                return new SearchBookCommand(serviceFactory.getBookService());
            case CommandName.SORT_BOOK:
                return new SortBookCommand(serviceFactory.getBookService());
            default:
                throw new IllegalArgumentException(CommandName.ILLEGAL_COMMAND);
        }
    }

    @Override
    public void close() {
        ConnectionPool.getInstance().returnConnection(connection);
    }
}
