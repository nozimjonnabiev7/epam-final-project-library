package com.nozimjon.finalproject.controller.builder;

import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * Builder class to construct Order objects from HttpServletRequest.
 */
public class OrderBuilderFromRequest {

    /**
     * Builds an Order object for updating based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the order information.
     * @return The Order object with the updated data.
     */
    public Order buildToUpdate(HttpServletRequest request) {
        String orderId = request.getParameter(OrderConstant.ORDER_ID);
        String bookReturned = request.getParameter(OrderConstant.BOOK_RETURNED);

        Order order = buildToAdd(request);
        order.setOrderId(Long.parseLong(orderId));
        order.setBookReturned(Boolean.parseBoolean(bookReturned.trim()));

        return order;
    }

    /**
     * Builds an Order object for adding a new order based on the data in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object containing the new order information.
     * @return The Order object with the data for the new order.
     */
    public Order buildToAdd(HttpServletRequest request) {

        String bookId = request.getParameter(OrderConstant.BOOK_ID);
        String userId = request.getParameter(OrderConstant.USER_ID);
        String orderDate = request.getParameter(OrderConstant.ORDER_DATE);
        String returningDate = request.getParameter(OrderConstant.RETURNING_DATE);
        String readingPlace = request.getParameter(OrderConstant.READING_PLACE);

        return createOrder(Long.parseLong(bookId), Long.parseLong(userId), Date.valueOf(orderDate),
                Date.valueOf(returningDate), readingPlace);
    }

    /**
     * Builds an Order object for a user order based on the data in the HttpServletRequest, bookId, and userId.
     *
     * @param request   The HttpServletRequest object containing the user order information.
     * @param bookId    The book ID for the order.
     * @param userId    The user ID for the order.
     * @return The Order object with the data for the user order.
     */
    public Order userOrder(HttpServletRequest request, Long bookId, Long userId) {

        String orderDate = request.getParameter(OrderConstant.ORDER_DATE);
        String returningDate = request.getParameter(OrderConstant.RETURNING_DATE);
        String readingPlace = request.getParameter(OrderConstant.READING_PLACE);

        return createOrder(bookId, userId, Date.valueOf(orderDate),
                Date.valueOf(returningDate), readingPlace);
    }

    private Order createOrder(Long bookId, Long userId, Date orderDate, Date returningDate, String readingPlace) {
        return new Order(bookId, userId, orderDate, returningDate, EnumService.getReadingPlace(readingPlace));
    }
}
