package com.nozimjon.finalproject.controller.command;

import com.nozimjon.finalproject.controller.builder.UserBuilderFromRequest;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.validator.entity.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle user registration (sign up) in the system.
 */
public class SignUpCommand implements Command {

    private final static Logger logger = LogManager.getLogger();
    private final UserService userService;
    private final UserBuilderFromRequest builderFromRequest = new UserBuilderFromRequest();

    /**
     * Constructs a SignUpCommand with the specified UserService.
     *
     * @param userService The UserService to be used for user registration.
     */
    public SignUpCommand(UserService userService) {
        this.userService = userService;
    }

    /**
     * Executes the sign-up command, handling user registration in the system.
     *
     * @param request  The HttpServletRequest object.
     * @param response The HttpServletResponse object.
     * @return The CommandResult indicating the next action after execution.
     * @throws ServiceException If there is a service-level exception during user registration.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CommandResult commandResult = new CommandResult();
        List<String> userValidation = UserValidator.validateUserParameter(request);

        if (userValidation.isEmpty()) {
            User user = builderFromRequest.buildUserForInserting(request);
            try {
                userService.save(user);
            } catch (ServiceException e) {
                logger.error(e);
            } finally {
                commandResult.redirect(PageLocation.LOGIN_PAGE);
            }
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, userValidation);
            commandResult.forward(PageLocation.REGISTRATION_PAGE);
        }
        return commandResult;
    }
}
