package com.nozimjon.finalproject.controller.command.user.order;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * A command to order a book.
 */
public class OrderBookCommand implements Command {

    private final BookService bookService;

    public OrderBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request,
                                 HttpServletResponse response) throws ServiceException {


        String destinationPage = null;
        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        Optional<Book> optionalBook = bookService.getById(Long.parseLong(bookId));

        if (optionalBook.isPresent()) {
            Book book = optionalBook.orElse(null);
            if (user != null) {
                request.setAttribute(BookConstant.BOOK_ATTRIBUTE, book);
                request.setAttribute(DiffConstant.DISPLAY, DiffConstant.CONFIRM);
                destinationPage = PageLocation.VIEW_BOOK;
            } else {
                request.setAttribute(BookConstant.BOOK_ATTRIBUTE, book);
                request.setAttribute(UserConstant.INVALID_LOGIN, DiffConstant.READ_FROM_PROPERTIES);
                destinationPage = PageLocation.VIEW_BOOK;
            }
        }

        return new CommandResult(destinationPage);
    }
}
