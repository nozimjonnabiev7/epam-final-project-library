package com.nozimjon.finalproject.controller.command;

import com.nozimjon.finalproject.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Command interface represents a command pattern for handling HTTP requests in a web application.
 * Implementing classes should provide specific command logic based on the incoming HTTP request.
 */
public interface Command {

    /**
     * Executes the specific command logic based on the incoming HTTP request.
     *
     * @param request  the HttpServletRequest object containing the request parameters and attributes.
     * @param response the HttpServletResponse object to set the response content.
     * @return a CommandResult object containing the page location to forward or redirect.
     * @throws ServiceException if an error occurs during the service operation.
     */
    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException;
}
