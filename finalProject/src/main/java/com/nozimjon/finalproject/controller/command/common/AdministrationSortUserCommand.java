package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * A command to sort the list of users in the administration panel based on different criteria.
 */
public class AdministrationSortUserCommand implements Command {

    private static final String SORT_CRITERIA = "type";
    private final UserService userService;

    /**
     * Constructs the AdministrationSortUserCommand.
     *
     * @param userService The UserService to use for fetching users.
     */
    public AdministrationSortUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);
        String sortCriteria = request.getParameter(SORT_CRITERIA);

        List<User> userList = sortResult(sortCriteria, user);

        request.setAttribute(UserConstant.USER_LIST, userList);

        return new CommandResult(PageLocation.ADMINISTRATION_USER_LIST);
    }

    /**
     * Sorts the list of users based on the specified criteria.
     *
     * @param sortCriteria The criteria for sorting users.
     * @param user         The current user making the request.
     * @return The sorted list of users.
     * @throws ServiceException If there is an error while fetching or sorting users.
     */
    private List<User> sortResult(String sortCriteria, User user) throws ServiceException {
        List<User> userList = null;

        if (sortCriteria.equalsIgnoreCase(UserConstant.NAME)) {
            if (user.getRole().equals(Roles.ADMIN)) {
                userList = userService.adminSortUsersByName();
            } else {
                userList = userService.sortUsersByName();
            }
        } else if (sortCriteria.equalsIgnoreCase(UserConstant.EMAIL)) {
            if (user.getRole().equals(Roles.ADMIN)) {
                userList = userService.adminSortUsersByEmail();
            } else {
                userList = userService.sortUsersByEmail();
            }
        }

        return userList;
    }
}
