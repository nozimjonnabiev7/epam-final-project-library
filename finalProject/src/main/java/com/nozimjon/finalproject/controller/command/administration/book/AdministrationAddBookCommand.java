package com.nozimjon.finalproject.controller.command.administration.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle adding a book in the administration section.
 */
public class AdministrationAddBookCommand implements Command {

    private final BookService bookService;

    /**
     * Constructs an AdministrationAddBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public AdministrationAddBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        List<Book> books = bookService.getAll();
        request.setAttribute(BookConstant.BOOK_LIST, books);
        return new CommandResult(PageLocation.ADMINISTRATION_BOOK_STORE);
    }
}
