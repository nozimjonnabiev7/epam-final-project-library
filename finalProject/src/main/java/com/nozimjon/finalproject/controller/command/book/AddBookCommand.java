package com.nozimjon.finalproject.controller.command.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle adding a book and displaying the book store page.
 */
public class AddBookCommand implements Command {

    private final BookService bookService;

    /**
     * Constructs an AddBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public AddBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * Executes the command to retrieve all books and display the book store page.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        List<Book> books = bookService.getAll();
        request.setAttribute(BookConstant.BOOK_LIST, books);

        return new CommandResult(PageLocation.BOOK_STORE);
    }
}
