package com.nozimjon.finalproject.controller.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * The EncodingFilter is responsible for setting the character encoding of incoming requests and responses.
 * It ensures that all data transmitted between the client and server is encoded in the specified character set.
 */
public class EncodingFilter implements Filter {

    private static final String ENCODING_PARAMETER = "encoding";
    private String charset;

    /**
     * Initializes the filter by retrieving the character encoding from the filter configuration.
     *
     * @param filterConfig the FilterConfig object containing filter configuration
     * @throws ServletException if an exception occurs during initialization
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        charset = filterConfig.getInitParameter(ENCODING_PARAMETER);
    }

    /**
     * Sets the character encoding of the request and response, and then continues the filter chain.
     *
     * @param servletRequest  the ServletRequest object
     * @param servletResponse the ServletResponse object
     * @param filterChain     the FilterChain object
     * @throws IOException      if an I/O error occurs
     * @throws ServletException if the request could not be handled
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String requestCharset = servletRequest.getCharacterEncoding();
        if (charset != null && !charset.equalsIgnoreCase(requestCharset)) {
            servletRequest.setCharacterEncoding(charset);
            servletResponse.setCharacterEncoding(charset);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /**
     * Destroys the filter.
     */
    @Override
    public void destroy() {

    }
}
