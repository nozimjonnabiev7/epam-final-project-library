package com.nozimjon.finalproject.controller.command.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle sorting the list of books based on criteria and displaying the sorted results.
 */
public class SortBookCommand implements Command {

    private static final String SORT_CRITERIA = "type";

    private final BookService bookService;

    /**
     * Constructs a SortBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public SortBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * Executes the command to sort the list of books based on the provided criteria and displays the sorted results.
     *
     * @param request  The HttpServletRequest containing user information.
     * @param response The HttpServletResponse used for server response.
     * @return The CommandResult that represents the next view and redirection status.
     * @throws ServiceException If there is an error during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);
        String sortCriteria = request.getParameter(SORT_CRITERIA);
        List<Book> bookList = sortedBookList(sortCriteria);
        request.setAttribute(BookConstant.BOOK_LIST, bookList);
        return commandResult(user);
    }

    /**
     * Sorts the list of books based on the specified sort criteria.
     *
     * @param sortCriteria The criteria to sort the books (e.g., name, quantity).
     * @return The sorted list of books.
     * @throws ServiceException If there is an error during the service operation.
     */
    private List<Book> sortedBookList(String sortCriteria) throws ServiceException {
        List<Book> bookList = null;
        if (sortCriteria.equalsIgnoreCase(BookConstant.BOOK_NAME)) {
            bookList = bookService.sortBooksByName();
        } else if (sortCriteria.equalsIgnoreCase(BookConstant.BOOK_QUANTITY)) {
            bookList = bookService.sortBookByQuantity();
        }
        return bookList;
    }

    /**
     * Determines the appropriate page to redirect after executing the command based on the user's role.
     *
     * @param user The user object representing the current user.
     * @return The CommandResult that represents the next view and redirection status.
     */
    private CommandResult commandResult(User user) {
        String page;
        if (user == null) {
            page = PageLocation.BOOK_STORE;
        } else if (user.getRole().equals(Roles.ADMIN) || user.getRole().equals(Roles.LIBRARIAN)) {
            page = PageLocation.ADMINISTRATION_BOOK_STORE;
        } else {
            page = PageLocation.BOOK_STORE;
        }
        return new CommandResult(page);
    }
}
