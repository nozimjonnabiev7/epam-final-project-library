package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.List;

/**
 * A command to sort the list of orders in the administration panel based on different criteria.
 */
public class AdministrationSortOrderCommand implements Command {

    private static final String SORT_CRITERIA = "type";
    private static final String BOOK_NAME = "bookName";
    private static final String USER_NAME = "userName";
    private static final String USER_EMAIL = "email";
    private static final String ORDER_DATE = "orderDate";
    private static final String RETURNING_DATE = "returningDate";
    private static final String READING_PLACE = "readingPlace";

    private final OrderService orderService;

    /**
     * Constructs the AdministrationSortOrderCommand.
     *
     * @param orderService The OrderService to use for fetching orders.
     */
    public AdministrationSortOrderCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String sortCriteria = request.getParameter(SORT_CRITERIA);
        List<AdministrationOrderDisplay> orders = orderService.administrationAllOrder();
        sort(sortCriteria, orders);
        request.setAttribute(OrderConstant.ORDER_LIST, orders);
        return new CommandResult(PageLocation.ADMINISTRATION_ORDER_LIST);
    }

    /**
     * Sorts the list of orders based on the specified criteria.
     *
     * @param criteria        The criteria for sorting orders.
     * @param orderDisplayList The list of orders to be sorted.
     */
    private void sort(String criteria, List<AdministrationOrderDisplay> orderDisplayList) {
        switch (criteria) {
            case BOOK_NAME:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getBookName));
                break;
            case USER_NAME:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getUserName));
                break;
            case USER_EMAIL:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getUserEmail));
                break;
            case ORDER_DATE:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getOrderDate));
                break;
            case RETURNING_DATE:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getReturningDate));
                break;
            case READING_PLACE:
                orderDisplayList.sort(Comparator.comparing(AdministrationOrderDisplay::getReadingPlace));
                break;
        }
    }
}
