package com.nozimjon.finalproject.controller.command.administration.order;

import com.nozimjon.finalproject.controller.builder.OrderBuilderFromRequest;
import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.TransactionManager;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;
import com.nozimjon.finalproject.util.validator.entity.OrderValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle updating or inserting orders in the administration section.
 */
public class AdministrationUpdateOrderCommand implements Command {

    private final static Logger logger = LogManager.getLogger();

    private final static String TRUE = "true";
    private final static String FALSE = "false";

    private final OrderService orderService;
    private final BookService bookService;
    private final TransactionManager transactionManager;
    private final OrderBuilderFromRequest builderFromRequest = new OrderBuilderFromRequest();

    /**
     * Constructs an AdministrationUpdateOrderCommand with the specified services.
     *
     * @param orderService       The OrderService used to handle order-related operations.
     * @param bookService        The BookService used to handle book-related operations.
     * @param transactionManager The TransactionManager used to manage transactions.
     */
    public AdministrationUpdateOrderCommand(OrderService orderService, BookService bookService, TransactionManager transactionManager) {
        this.orderService = orderService;
        this.bookService = bookService;
        this.transactionManager = transactionManager;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        CommandResult commandResult;
        String orderId = request.getParameter(OrderConstant.ORDER_ID);
        String bookReturned = request.getParameter(OrderConstant.BOOK_RETURNED);
        String bookId = request.getParameter(OrderConstant.BOOK_ID);

        if ((orderId != null) && bookReturned.trim().equalsIgnoreCase(FALSE) && !orderId.isEmpty()) {
            commandResult = updateOrder(request);
        } else if ((orderId != null) && bookReturned.trim().equalsIgnoreCase(TRUE) && !orderId.isEmpty()) {
            commandResult = removeOrder(request, orderId, bookId);
        } else {
            commandResult = insertOrder(request);
        }
        return commandResult;
    }

    private CommandResult updateOrder(HttpServletRequest request) {
        String operation = null;
        CommandResult commandResult = new CommandResult();
        List<String> orderValidation = OrderValidator.validateOrderParameter(request);
        if (orderValidation.isEmpty()) {
            Order order = builderFromRequest.buildToUpdate(request);
            try {
                orderService.update(order);
                operation = Operation.UPDATED;
            } catch (ServiceException e) {
                operation = Operation.UPDATE_FAIL;
                logger.error(e);
            } finally {
                commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_ORDER_PAGE + Operation.OPERATION_STATUS + operation);
            }
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, orderValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_ORDER);
        }
        return commandResult;
    }

    private CommandResult insertOrder(HttpServletRequest request) {
        String operation = null;
        CommandResult commandResult = new CommandResult();
        List<String> orderValidation = OrderValidator.validateOrderParameter(request);
        if (orderValidation.isEmpty()) {
            Order order = builderFromRequest.buildToAdd(request);
            try {
                orderService.save(order);
                operation = Operation.INSERTED;
            } catch (ServiceException e) {
                operation = Operation.INSERT_FAIL;
                logger.error(e);
            } finally {
                commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_ORDER_PAGE + Operation.OPERATION_STATUS + operation);
            }
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, orderValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_ORDER);
        }
        return commandResult;
    }

    private CommandResult removeOrder(HttpServletRequest request, String orderId, String bookId) {
        String operation = null;
        CommandResult commandResult = new CommandResult();
        try {
            orderService.administrationOrderRemoval(orderId, bookId, bookService, transactionManager);
            operation = Operation.REMOVED;
        } catch (ServiceException e) {
            operation = Operation.REMOVE_FAIL;
            logger.error(e);
        } finally {
            commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_ORDER_PAGE + Operation.OPERATION_STATUS + operation);
        }
        return commandResult;
    }
}
