package com.nozimjon.finalproject.controller.command.common;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A command to navigate to the user's profile page.
 */
public class ProfileCommand implements Command {

    /**
     * Executes the command to navigate to the user's profile page.
     *
     * @param request  The HTTP servlet request.
     * @param response The HTTP servlet response.
     * @return The CommandResult with the page location for the user's profile.
     * @throws ServiceException If there is an error while executing the command.
     */
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        return new CommandResult(PageLocation.PROFILE);
    }
}
