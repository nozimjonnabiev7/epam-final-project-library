package com.nozimjon.finalproject.controller.command;

/**
 * Represents the result of a command execution, indicating the next page
 * to be displayed and the action to be taken (redirect or forward).
 */
public class CommandResult {

    private String page;
    private CommandAction commandAction = CommandAction.FORWARD;

    /**
     * Default constructor for CommandResult.
     */
    public CommandResult() {
    }

    /**
     * Constructor for CommandResult that sets the page to display.
     *
     * @param page the page to display.
     */
    public CommandResult(String page) {
        this.page = page;
    }


    public String getPage() {
        return page;
    }

    private void setPage(String page) {
        this.page = page;
    }

    public CommandAction getCommandAction() {
        return commandAction;
    }

    /**
     * Sets the page and action to redirect to another page.
     *
     * @param page the page to redirect to.
     */
    public void redirect(String page) {
        setPage(page);
        commandAction = CommandAction.REDIRECT;
    }

    /**
     * Sets the page and action to forward to another page.
     *
     * @param page the page to forward to.
     */
    public void forward(String page) {
        setPage(page);
        commandAction = CommandAction.FORWARD;
    }
}
