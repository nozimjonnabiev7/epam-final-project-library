package com.nozimjon.finalproject.controller.command;

import com.nozimjon.finalproject.util.constant.PageLocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command to handle user logout from the system.
 */
public class LogOutCommand implements Command {

    /**
     * Executes the logout action by invalidating the current session.
     * After the logout, the user is redirected to the login page.
     *
     * @param request  the HTTP servlet request
     * @param response the HTTP servlet response
     * @return the CommandResult instance containing the login page location for redirection
     */

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return new CommandResult(PageLocation.LOGIN_PAGE);
    }
}
