package com.nozimjon.finalproject.controller.command;

public enum CommandAction {

    REDIRECT,
    FORWARD
}
