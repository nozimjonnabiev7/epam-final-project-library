package com.nozimjon.finalproject.controller.command;

import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.service.UserService;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

public class LoginCommand implements Command {

    private final UserService userService;

    public LoginCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String page;
        CommandResult commandResult = new CommandResult();
        HttpSession session = request.getSession();
        setBuildAndTimeStamp(session);

        String login = request.getParameter(UserConstant.LOGIN);
        String password = request.getParameter(UserConstant.PASSWORD);
        Optional<User> optionalUser = userService.findByLoginPassword(login, password);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (!user.isBlocked()) {
                session.setAttribute(UserConstant.USER_ATTRIBUTE, user);
                page = PageLocation.PROFILE;
            } else {
                // User is blocked, show a message on the login page
                request.setAttribute(UserConstant.BLOCK_MESSAGE, DiffConstant.READ_FROM_PROPERTIES);
                page = PageLocation.LOGIN_PAGE;
            }
        } else {
            // Invalid login credentials, show a message on the login page
            request.setAttribute(UserConstant.INVALID_LOGIN, DiffConstant.READ_FROM_PROPERTIES);
            page = PageLocation.LOGIN_PAGE;
        }

        commandResult.forward(page);
        return commandResult;
    }

    private void setBuildAndTimeStamp(HttpSession session) {
        // Load buildInfo.properties to get version and timestamp
        InputStream in = getClass().getClassLoader().getResourceAsStream("buildInfo.properties");
        if (in == null) {
            return;
        }

        Properties props = new Properties();
        try {
            props.load(in);
            String version = props.getProperty("build.version");
            String timeStamp = props.getProperty("build.timestamp");

            session.setAttribute("version", version);
            session.setAttribute("timeStamp", timeStamp);
        } catch (IOException e) {
            // Handle the exception gracefully, e.g., logging the error
            e.printStackTrace();
        }
    }
}
