package com.nozimjon.finalproject.controller.command.admin;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle removing a book by the administrator.
 */
public class AdminRemoveBookCommand implements Command {

    private static final Logger logger = LogManager.getLogger();

    private final BookService bookService;

    /**
     * Constructs an AdminRemoveBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public AdminRemoveBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        CommandResult commandResult = new CommandResult();
        String operation;
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        List<Book> books = bookService.getAll();
        try {
            bookService.removeById(Long.parseLong(bookId));
            operation = Operation.REMOVED;
        } catch (ServiceException e) {
            operation = Operation.REMOVE_FAIL;
            logger.error("Error while removing book", e);
        }
        request.setAttribute(BookConstant.BOOK_LIST, books);
        commandResult.redirect(RedirectTo.ADMINISTRATION_BOOK_STORE_PAGE + Operation.OPERATION_STATUS + operation);
        return commandResult;
    }
}
