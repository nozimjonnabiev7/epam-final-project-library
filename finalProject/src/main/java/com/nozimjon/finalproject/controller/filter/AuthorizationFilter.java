package com.nozimjon.finalproject.controller.filter;

import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.util.constant.CommandName;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * The AuthorizationFilter is a filter that performs authorization checks on incoming requests based on user roles.
 * It ensures that only authorized users can access certain commands in the system.
 */
@WebFilter("/controller")
public class AuthorizationFilter implements Filter {

    private static final List<String> ADMINISTRATION_COMMANDS = Arrays.asList(
            CommandName.PROFILE,

            CommandName.ADMINISTRATION_BOOK_STORE,
            CommandName.ADMINISTRATION_EDIT_BOOK,
            CommandName.ADMINISTRATION_UPDATE_BOOK,

            CommandName.ADMINISTRATION_ORDER_LIST,
            CommandName.ADMINISTRATION_EDIT_ORDER,
            CommandName.ADMINISTRATION_UPDATE_ORDER,
            CommandName.ADMINISTRATION_SORT_ORDER,
            CommandName.ADMINISTRATION_SEARCH_ORDER,

            CommandName.ADMINISTRATION_DISPLAY_USER,
            CommandName.ADMINISTRATION_EDIT_USER,
            CommandName.ADMINISTRATION_UPDATE_USER,
            CommandName.ADMINISTRATION_SEARCH_USER,
            CommandName.ADMINISTRATION_SORT_USER,

            // Admin Only
            CommandName.ADMIN_REMOVE_BOOK,
            CommandName.ADMIN_REMOVE_USER
    );

    private final static List<String> USER_COMMANDS = Arrays.asList(
            CommandName.CONFIRM_ORDER,
            CommandName.USER_ORDER,
            CommandName.PROFILE,
            CommandName.DISPLAY_BOOK

    );

    private final static List<String> COMMON_COMMANDS = Arrays.asList(
            CommandName.REGISTRATION,
            CommandName.LOGIN,
            CommandName.LOGOUT,
            CommandName.DISPLAY_BOOK,
            CommandName.VIEW_BOOK,
            CommandName.CHANGE_LANGUAGE,
            CommandName.ORDER_BOOK,
            CommandName.SEARCH_BOOK,
            CommandName.SORT_BOOK
    );

    @Override
    public void init(FilterConfig filterConfig) {

    }

    /**
     * Performs the authorization check on incoming requests and allows access to authorized users only.
     *
     * @param servletRequest  the ServletRequest object
     * @param servletResponse the ServletResponse object
     * @param filterChain     the FilterChain object
     * @throws IOException      if an I/O error occurs
     * @throws ServletException if the request could not be handled
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserConstant.USER_ATTRIBUTE);
        String command = request.getParameter(CommandName.COMMAND_NAME);

        if (!isCommonCommand(command)) {
            if (user != null && user.getRole() == Roles.LIBRARIAN && isAdministrationCommand(command)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else if (user != null && user.getRole() == Roles.ADMIN && isAdministrationCommand(command)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else if (user != null && user.getRole() == Roles.READER && isUserCommand(command)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                response.sendRedirect(request.getContextPath() + RedirectTo.LOGIN_PAGE);
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }

    private boolean isAdministrationCommand(String command) {
        return ADMINISTRATION_COMMANDS.contains(command);
    }

    private boolean isUserCommand(String command) {
        return USER_COMMANDS.contains(command);
    }

    private boolean isCommonCommand(String command) {
        return COMMON_COMMANDS.contains(command);
    }
}
