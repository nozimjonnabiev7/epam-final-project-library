package com.nozimjon.finalproject.controller.command.administration.book;

import com.nozimjon.finalproject.controller.builder.BookBuilderFromRequest;
import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.Operation;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.RedirectTo;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.validator.entity.BookValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Command to handle updating or inserting a book in the administration section.
 */
public class AdministrationUpdateBookCommand implements Command {

    private static final Logger logger = LogManager.getLogger();
    private final BookService bookService;
    private final BookBuilderFromRequest builderFromRequest = new BookBuilderFromRequest();

    /**
     * Constructs an AdministrationUpdateBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public AdministrationUpdateBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {

        CommandResult commandResult;
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        if ((bookId != null) && !bookId.isEmpty()) {
            commandResult = updateBook(request);
        } else {
            commandResult = insertBook(request);
        }
        return commandResult;
    }

    private CommandResult updateBook(HttpServletRequest request) {
        String operation;
        CommandResult commandResult = new CommandResult();
        List<String> bookValidation = BookValidator.validateBookParameter(request);
        if (bookValidation.isEmpty()) {
            Book book = builderFromRequest.buildBookToUpdate(request);
            try {
                bookService.update(book);
                operation = Operation.UPDATED;
            } catch (ServiceException e) {
                operation = Operation.UPDATE_FAIL;
                logger.error("Error while updating book", e);
            }
            commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_BOOK_PAGE + Operation.OPERATION_STATUS + operation);
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, bookValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_BOOK);
        }

        return commandResult;
    }

    private CommandResult insertBook(HttpServletRequest request) {
        String operation;
        CommandResult commandResult = new CommandResult();
        List<String> bookValidation = BookValidator.validateBookParameter(request);
        if (bookValidation.isEmpty()) {
            Book book = builderFromRequest.buildBookToAdd(request);
            try {
                bookService.save(book);
                operation = Operation.INSERTED;
            } catch (ServiceException e) {
                operation = Operation.INSERT_FAIL;
                logger.error("Error while inserting book", e);
            }
            commandResult.redirect(request.getContextPath() + RedirectTo.ADMINISTRATION_EDIT_BOOK_PAGE + Operation.OPERATION_STATUS + operation);
        } else {
            request.setAttribute(Operation.VALIDATION_LIST, bookValidation);
            commandResult.forward(request.getContextPath() + PageLocation.ADMINISTRATION_EDIT_BOOK);
        }

        return commandResult;
    }
}
