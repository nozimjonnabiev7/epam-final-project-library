package com.nozimjon.finalproject.controller.command.administration.book;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.service.BookService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.DiffConstant;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Command to handle editing a book in the administration section.
 */
public class AdministrationEditBookCommand implements Command {

    private final BookService bookService;

    /**
     * Constructs an AdministrationEditBookCommand with the specified BookService.
     *
     * @param bookService The BookService used to handle book-related operations.
     */
    public AdministrationEditBookCommand(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String page;
        String bookId = request.getParameter(BookConstant.BOOK_ID);
        if (bookId != null) {
            Optional<Book> optionalBook = bookService.getById(Long.parseLong(bookId));
            if (optionalBook.isPresent()) {
                Book book = optionalBook.get();
                request.setAttribute(BookConstant.EDIT_BOOK, book);
                page = PageLocation.ADMINISTRATION_EDIT_BOOK;
            } else {
                request.setAttribute(BookConstant.BOOK_NOT_EXIST, DiffConstant.READ_FROM_PROPERTIES);
                page = PageLocation.ADMINISTRATION_BOOK_STORE;
            }
        } else {
            page = PageLocation.ADMINISTRATION_EDIT_BOOK;
        }

        return new CommandResult(page);
    }
}
