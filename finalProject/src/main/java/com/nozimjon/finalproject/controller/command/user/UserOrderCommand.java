package com.nozimjon.finalproject.controller.command.user;

import com.nozimjon.finalproject.controller.command.Command;
import com.nozimjon.finalproject.controller.command.CommandResult;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.dto.UserOrderDisplay;
import com.nozimjon.finalproject.service.OrderService;
import com.nozimjon.finalproject.service.ServiceException;
import com.nozimjon.finalproject.util.constant.PageLocation;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@code UserOrderCommand} class is a command to display the orders of the logged-in user.
 */
public class UserOrderCommand implements Command {

    private final OrderService orderService;

    /**
     * Constructs a new UserOrderCommand with the specified OrderService.
     *
     * @param orderService the OrderService to handle order-related operations.
     */
    public UserOrderCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * Executes the command to display the orders of the logged-in user.
     *
     * @param request  the HttpServletRequest object containing the request parameters and attributes.
     * @param response the HttpServletResponse object to set the response content.
     * @return a CommandResult object containing the page location to forward or redirect.
     * @throws ServiceException if an error occurs during the service operation.
     */
    @Override
    public CommandResult execute(HttpServletRequest request,
                                 HttpServletResponse response) throws ServiceException {

        User user = (User) request.getSession(false).getAttribute(UserConstant.USER_ATTRIBUTE);
        List<UserOrderDisplay> userOrderDisplays = orderService.userOrders();

        // Filter the orders for the logged-in user
        List<UserOrderDisplay> orderList = userOrderDisplays.stream()
                .filter(userOrderDisplay -> userOrderDisplay.getUserId() == user.getId())
                .collect(Collectors.toList());

        request.setAttribute(OrderConstant.ORDER_LIST, orderList);
        return new CommandResult(PageLocation.USER_ORDER);
    }
}
