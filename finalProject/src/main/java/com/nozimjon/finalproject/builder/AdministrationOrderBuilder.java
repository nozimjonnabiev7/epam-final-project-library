package com.nozimjon.finalproject.builder;

import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class to construct LibrarianOrderDisplay objects from ResultSet.
 */
public class AdministrationOrderBuilder implements Builder<AdministrationOrderDisplay> {
    private static final String BOOK_NAME = "book_title";
    private static final String USER_NAME = "first_name";
    private static final String USER_EMAIL = "user_email";

    @Override
    public AdministrationOrderDisplay build(ResultSet resultSet) throws SQLException {

        long orderId = resultSet.getLong(OrderConstant.ORDER_ID);
        String bookName = resultSet.getString(BOOK_NAME);
        String userName = resultSet.getString(USER_NAME);
        String userEmail = resultSet.getString(USER_EMAIL);

        Date orderDate = resultSet.getDate(OrderConstant.ORDER_DATE);
        Date returningDate = resultSet.getDate(OrderConstant.RETURNING_DATE);
        Array readingPlaceArray = resultSet.getArray(OrderConstant.READING_PLACE);
        String[] readingPlaceValues = (String[]) readingPlaceArray.getArray();

        ReadingPlace readingPlace = EnumService.getReadingPlace(readingPlaceValues[0]);
        boolean bookReturned = resultSet.getBoolean(OrderConstant.BOOK_RETURNED);

        return new AdministrationOrderDisplay(orderId, bookName, userName, userEmail, orderDate, returningDate, readingPlace, bookReturned);
    }
}
