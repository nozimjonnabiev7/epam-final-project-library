package com.nozimjon.finalproject.builder;

import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
import com.nozimjon.finalproject.dto.UserOrderDisplay;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class to construct UserOrderDisplay objects from ResultSet.
 */
public class UserOrderBuilder implements Builder<UserOrderDisplay> {

    private static final String BOOK_NAME_COLUMN = "book_title";

    @Override
    public UserOrderDisplay build(ResultSet resultSet) throws SQLException {

        long orderId = resultSet.getLong(OrderConstant.ORDER_ID);
        long userId = resultSet.getLong(OrderConstant.USER_ID);
        String bookName = resultSet.getString(BOOK_NAME_COLUMN);
        Date orderDate = resultSet.getDate(OrderConstant.ORDER_DATE);
        Date returningDate = resultSet.getDate(OrderConstant.RETURNING_DATE);
        Array readingPlaceArray = resultSet.getArray(OrderConstant.READING_PLACE);
        String[] readingPlaceValues = (String[]) readingPlaceArray.getArray();
        ReadingPlace readingPlace = EnumService.getReadingPlace(readingPlaceValues[0]);
        boolean bookReturned = resultSet.getBoolean(OrderConstant.BOOK_RETURNED);

        return new UserOrderDisplay(orderId, userId, bookName, orderDate, returningDate, readingPlace, bookReturned);
    }
}
