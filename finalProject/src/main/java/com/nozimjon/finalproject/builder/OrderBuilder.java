package com.nozimjon.finalproject.builder;

import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.constant.entity.OrderConstant;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class to construct Order objects from ResultSet.
 */
public class OrderBuilder implements Builder<Order> {

    @Override
    public Order build(ResultSet resultSet) throws SQLException {

        DataValidator.validateNotNull(resultSet, "Result set cannot be null in OrderBuilder.");

        int orderId = resultSet.getInt(OrderConstant.ORDER_ID);
        int bookId = resultSet.getInt(OrderConstant.BOOK_ID);
        int userId = resultSet.getInt(OrderConstant.USER_ID);
        Date orderDate = resultSet.getDate(OrderConstant.ORDER_DATE);
        Date returningDate = resultSet.getDate(OrderConstant.RETURNING_DATE);
        Array placeArray = resultSet.getArray(OrderConstant.READING_PLACE);
        String[] readingPlaceValues = (String[]) placeArray.getArray();
        ReadingPlace readingPlace = EnumService.getReadingPlace(readingPlaceValues[0]);
        boolean bookReturned = resultSet.getBoolean(OrderConstant.BOOK_RETURNED);

        return new Order(orderId, bookId, userId, orderDate, returningDate, readingPlace, bookReturned);
    }
}
