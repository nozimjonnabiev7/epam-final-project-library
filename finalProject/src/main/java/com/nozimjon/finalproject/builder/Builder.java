package com.nozimjon.finalproject.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Generic Builder interface to construct objects from a ResultSet.
 *
 * @param <T> The type of object to be built.
 */

public interface Builder<T> {
    /**
     * Builds an object of type T from the given ResultSet.
     *
     * @param resultSet The ResultSet containing the data to build the object.
     * @return The constructed object of type T.
     * @throws SQLException If there is an error accessing the ResultSet or building the object.
     */
    T build(ResultSet resultSet) throws SQLException;
}

