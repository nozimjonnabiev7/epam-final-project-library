package com.nozimjon.finalproject.builder;

import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.util.constant.entity.BookConstant;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class to construct Book objects from ResultSet.
 */
public class BookBuilder implements Builder<Book> {

    @Override
    public Book build(ResultSet resultSet) throws SQLException {

        DataValidator.validateNotNull(resultSet, "Result set cannot be null in BookBuilder.");

        long id = resultSet.getLong(BookConstant.BOOK_ID);
        String bookName = resultSet.getString(BookConstant.BOOK_NAME);
        int quantity = resultSet.getInt(BookConstant.BOOK_QUANTITY);

        return new Book(id, bookName, quantity);
    }
}
