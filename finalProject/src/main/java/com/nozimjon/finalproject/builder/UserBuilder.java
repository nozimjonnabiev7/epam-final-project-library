package com.nozimjon.finalproject.builder;

import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.util.constant.entity.UserConstant;
import com.nozimjon.finalproject.util.EnumService;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Builder class to construct User objects from ResultSet.
 */
public class UserBuilder implements Builder<User> {

    @Override
    public User build(ResultSet resultSet) throws SQLException {

        DataValidator.validateNotNull(resultSet, "Result set cannot be null in UserBuilder.");

        long id = resultSet.getLong(UserConstant.ID);
        String name = resultSet.getString(UserConstant.NAME);
        String lastName = resultSet.getString(UserConstant.LAST_NAME);
        String email = resultSet.getString(UserConstant.EMAIL);
        String login = resultSet.getString(UserConstant.LOGIN);
        String password = resultSet.getString(UserConstant.PASSWORD);
        Array roleArray = resultSet.getArray(UserConstant.ROLE);
        String[] roleValues = (String[]) roleArray.getArray();
        Roles role = EnumService.getRole(roleValues[0]);
        boolean blocked = resultSet.getBoolean(UserConstant.BLOCKED);

        return new User(id, name, lastName, email, login, password, role, blocked);
    }
}
