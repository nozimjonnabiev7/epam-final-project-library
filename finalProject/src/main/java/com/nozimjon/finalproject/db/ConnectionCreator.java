package com.nozimjon.finalproject.db;

import com.nozimjon.finalproject.util.PropertiesExtractor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * A utility class to create database connections using properties from the configuration file.
 */
public class ConnectionCreator {

    private static final String DB_PROPERTIES_URI = "dataBase";
    private static final String DRIVER = "db.driver";
    private static final String NAME = "db.name";
    private static final String URI = "db.uri";
    private static final String LOGIN = "db.login";
    private static final String PASSWORD = "db.password";

    private final String dbName;
    private final String dbURI;
    private final String dbLogin;
    private final String dbPassword;

    /**
     * Constructs a ConnectionCreator with properties extracted from the configuration file.
     */
    public ConnectionCreator() {
        dbName = PropertiesExtractor.getValueFromProperties(NAME, DB_PROPERTIES_URI);
        dbURI = PropertiesExtractor.getValueFromProperties(URI, DB_PROPERTIES_URI);
        dbLogin = PropertiesExtractor.getValueFromProperties(LOGIN, DB_PROPERTIES_URI);
        dbPassword = PropertiesExtractor.getValueFromProperties(PASSWORD, DB_PROPERTIES_URI);
    }

    /**
     * Creates a database connection using the configured properties.
     *
     * @return The Connection object.
     * @throws SQLException If a database access error occurs.
     */
    public Connection create() throws SQLException {
        String driver = PropertiesExtractor.getValueFromProperties(DRIVER, DB_PROPERTIES_URI);
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Database driver not found.", e);
        }
        return DriverManager.getConnection(dbURI + dbName, dbLogin, dbPassword);
    }
}
