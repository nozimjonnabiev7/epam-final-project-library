package com.nozimjon.finalproject.db;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A connection pool to manage and provide database connections.
 */
public class ConnectionPool {

    private static final int POOL_SIZE = 5;
    private static final Logger logger = LogManager.getLogger();
    private static final Lock lock = new ReentrantLock();
    private static final AtomicBoolean isCreated = new AtomicBoolean(false);
    private static ConnectionPool instance;
    private final LinkedBlockingQueue<Connection> connectionQueue;

    /**
     * Private constructor to create the connection pool and initialize connections.
     */
    private ConnectionPool() {
        logger.log(Level.INFO, "Connecting to DataBase......");
        connectionQueue = new LinkedBlockingQueue<>(POOL_SIZE);
        initConnections();
    }

    /**
     * Initializes the connection pool by creating database connections.
     */
    private void initConnections() {
        ConnectionCreator creator = new ConnectionCreator();
        try {
            connectionQueue.offer(creator.create());
        } catch (SQLException e) {
            logger.error("Can't get Connection", e);
        }

        if (connectionQueue.isEmpty()) {
            throw new IllegalStateException("No connection created");
        }
    }

    /**
     * Returns the instance of the connection pool using double-checked locking.
     *
     * @return The ConnectionPool instance.
     */
    public static ConnectionPool getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            try {
                if (!isCreated.get()) {
                    instance = new ConnectionPool();
                    isCreated.set(true);
                    logger.info("Pool Created successfully.");
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Retrieves a database connection from the pool.
     *
     * @return The Connection object.
     */
    public Connection getConnection() {
        try {
            return connectionQueue.take();
        } catch (InterruptedException e) {
            throw new IllegalStateException("Can't get the connection from the connectionQueue at ConnectionPool Class", e);
        }
    }

    /**
     * Returns the connection back to the pool.
     *
     * @param connection The Connection object to be returned.
     */
    public void returnConnection(Connection connection) {
        connectionQueue.offer(connection);
        logger.debug("Connection returned to the pool successfully");
    }

    /**
     * Closes all connections in the pool.
     */
    public void closePool() {
        while (!connectionQueue.isEmpty()) {
            try {
                Connection connection = connectionQueue.take();
                connection.close();
            } catch (SQLException e) {
                logger.warn("Can not close the connection", e);
            } catch (InterruptedException e) {
                logger.error("Can not close the pool", e);
            }
        }
    }
}
