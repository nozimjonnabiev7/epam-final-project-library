package com.nozimjon.finalproject.dao;

import java.util.List;
import java.util.Optional;

public interface IDAO<T> {

    Optional<T> getById(long id) throws DAOException;

    List<T> getAll() throws DAOException;

    void save(T item) throws DAOException;

    void removeById(long id) throws DAOException;

    void update(T item) throws DAOException;
}
