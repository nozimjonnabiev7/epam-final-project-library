package com.nozimjon.finalproject.dao;

import com.nozimjon.finalproject.builder.UserBuilder;
import com.nozimjon.finalproject.entity.User;
import com.nozimjon.finalproject.util.MD5Encryption;
import com.nozimjon.finalproject.util.constant.query.UserQuery;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

/**
 * Data Access Object (DAO) for handling database operations related to users.
 */
public class UserDAO extends AbstractDAO<User> {

    /**
     * Constructs a new UserDAO with the given database connection.
     *
     * @param connection the database connection
     */
    public UserDAO(final Connection connection) {
        super(connection);
    }

    /**
     * Default constructor.
     */
    public UserDAO() {

    }

    @Override
    public Optional<User> getById(final long id) throws DAOException {

        return executeSingleResponseQuery(UserQuery.SELECT_USER_BY_ID, new UserBuilder(), String.valueOf(id));
    }

    @Override
    public List<User> getAll() throws DAOException {
        return executeQuery(UserQuery.SELECT_ALL_USERS, new UserBuilder());
    }


    @Override
    public void save(final User item) throws DAOException {
        DataValidator.validateNotNull(item, "User item in save method at UserDao class can not be null");

        String encryptedPassword = MD5Encryption.encrypt(item.getPassword());
        String[] userInfo = { item.getName(), item.getLastName(), item.getEmail(), item.getLogin(), encryptedPassword };

        executeUpdate(UserQuery.INSERT_USER, userInfo);
    }

    @Override
    public void removeById(final long id) throws DAOException {
        executeUpdate(UserQuery.DELETE_USER, String.valueOf(id));
    }


    @Override
    public void update(final User item) throws DAOException {
        DataValidator.validateNotNull(item, "User item in update method at UserDao class can not be null");

        String blockStatus = String.valueOf(item.isBlocked());
        String[] userInfo = { item.getName(), item.getLastName(), item.getEmail(), item.getLogin(),
                item.getRole().name(), blockStatus, String.valueOf(item.getId()) };

        executeUpdate(UserQuery.UPDATE_USER, userInfo);

    }


    /**
     * Retrieves a user by their login and password.
     *
     * @param login    the login of the user
     * @param password the password of the user
     * @return an optional user based on the login and password
     * @throws DAOException if there's an error executing the database query
     */
    public Optional<User> findByLoginAndPassword(final String login, final String password) throws DAOException {
        DataValidator.validateNotEmptyString(login,
                "Not allow for a null or empty login in findByLoginAndPassword at UserDao class");
        DataValidator.validateNotEmptyString(login,
                "Not allow for a null or empty password in findByLoginAndPassword at UserDao class");
        String encryptedPassword = MD5Encryption.encrypt(password);
        System.out.println(this.getClass().getSimpleName() + ", encryptedPassword: " + encryptedPassword + " Password: "
                + password);
        return executeSingleResponseQuery(UserQuery.SELECT_USER_BY_LOGIN_PASSWORD, new UserBuilder(), login,
                encryptedPassword);
    }

    /**
     * Retrieves a user by their login.
     *
     * @param login the login of the user
     * @return an optional user based on the login
     * @throws DAOException if there's an error executing the database query
     */
    public Optional<User> findByLogin(final String login) throws DAOException {
        DataValidator.validateNotEmptyString(login,
                "Not allow for null or empty value in findByLogin " + "method at userDao Class");

        return executeSingleResponseQuery(UserQuery.SELECT_USER_BY_LOGIN, new UserBuilder(), login);
    }

    /**
     * Retrieves a user by their email.
     *
     * @param email the email of the user
     * @return an optional user based on the email
     * @throws DAOException if there's an error executing the database query
     */
    public Optional<User> findByEmail(final String email) throws DAOException {
        return executeSingleResponseQuery(UserQuery.SELECT_USER_BY_EMAIL, new UserBuilder(), email);
    }

    // Librarian Query

    /**
     * Retrieves a list of users with the role "READER".
     *
     * @return a list of users with the role "READER"
     * @throws DAOException if there's an error executing the database query
     */
    public List<User> findAllWhereRoleReader() throws DAOException {
        return executeQuery(UserQuery.SELECT_USER_BY_READER_ROLE, new UserBuilder());
    }

    /**
     * Retrieves a list of users sorted by name.
     *
     * @return a list of users sorted by name
     * @throws DAOException if there's an error executing the database query
     */
    public List<User> sortUsersByName() throws DAOException {
        return executeQuery(UserQuery.SELECT_ALL_USERS_SORTING_BY_NAME, new UserBuilder());
    }

    /**
     * Retrieves a list of users sorted by email.
     *
     * @return a list of users sorted by email
     * @throws DAOException if there's an error executing the database query
     */
    public List<User> sortUsersByEmail() throws DAOException {
        return executeQuery(UserQuery.SELECT_ALL_USERS_SORTING_BY_EMAIL, new UserBuilder());
    }

    // Admin Query

    /**
     * Retrieves a list of users sorted by name for administration.
     *
     * @return a list of users sorted by name for administration
     * @throws DAOException if there's an error executing the database query
     */
    public List<User> adminSortUsersByName() throws DAOException {
        return executeQuery(UserQuery.SELECT_ALL_SORTING_BY_NAME_FOR_ADMIN, new UserBuilder());
    }

    /**
     * Retrieves a list of users sorted by email for administration.
     *
     * @return a list of users sorted by email for administration
     * @throws DAOException if there's an error executing the database query
     */
    public List<User> adminSortUsersByEmail() throws DAOException {
        return executeQuery(UserQuery.SELECT_ALL_SORTING_BY_EMAIL_FOR_ADMIN, new UserBuilder());
    }

    /**
     * Updates the role of a user.
     *
     * @param id   the ID of the user
     * @param role the new role for the user
     * @throws DAOException if there's an error executing the database query
     */
    public void updateRole(final Long id, final String role) throws DAOException {
        executeUpdate(UserQuery.UPDATE_USER_ROLE, role, String.valueOf(id));
    }
}
