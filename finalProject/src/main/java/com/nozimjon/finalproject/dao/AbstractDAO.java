package com.nozimjon.finalproject.dao;

import com.nozimjon.finalproject.builder.Builder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The AbstractDAO class is an abstract base class that provides common functionality for data access objects (DAOs).
 * It implements the IDAO interface and provides methods for executing queries and updates in the database.
 *
 * @param <T> the type of entity that the DAO handles
 */
public abstract class AbstractDAO<T> implements IDAO<T> {

    private Connection connection;

    /**
     * Constructs a new AbstractDAO with the given database connection.
     *
     * @param connection the database connection
     */
    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Constructs a new AbstractDAO.
     */
    public AbstractDAO() {

    }

    /**
     * Gets the database connection associated with this DAO.
     *
     * @return the database connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Executes a query and returns a list of entities constructed using the provided builder.
     *
     * @param query      the SQL query to execute
     * @param builder    the builder to construct entities from the result set
     * @param parameters the query parameters
     * @return a list of entities
     * @throws DAOException if an error occurs while executing the query
     */
    protected List<T> executeQuery(String query, Builder<T> builder, String... parameters) throws DAOException {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        List<T> entities = new ArrayList<>();
        try {

            preparedStatement = connection.prepareStatement(query);
            prepareStatement(preparedStatement, parameters);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                entities.add(entity);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return entities;
    }

    /**
     * Executes a query and returns an Optional containing a single entity constructed using the provided builder.
     * If the query returns no results or more than one result, the Optional will be empty.
     *
     * @param query      the SQL query to execute
     * @param builder    the builder to construct the entity from the result set
     * @param parameters the query parameters
     * @return an Optional containing the single entity, or an empty Optional if the query returns no results or more than one result
     * @throws DAOException if an error occurs while executing the query
     */
    protected Optional<T> executeSingleResponseQuery(String query, Builder<T> builder, String... parameters) throws DAOException {
        List<T> list = executeQuery(query, builder, parameters);
        if (list.size() == 1) {
            return Optional.of(list.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Executes an update query (e.g., insert, update, delete) in the database.
     *
     * @param query      the SQL query to execute
     * @param parameters the query parameters
     * @throws DAOException if an error occurs while executing the query
     */
    protected void executeUpdate(String query, String... parameters) throws DAOException {
        PreparedStatement preparedStatement;

        try {

            preparedStatement = connection.prepareStatement(query);
            prepareStatement(preparedStatement, parameters);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Sets the parameters in the prepared statement.
     *
     * @param statement  the prepared statement
     * @param parameters the query parameters
     * @throws SQLException if an error occurs while setting the parameters
     */
    protected void prepareStatement(PreparedStatement statement, String... parameters) throws SQLException {
        for (int i = 1; i <= parameters.length; i++) {
            statement.setObject(i, parameters[i - 1]);
        }
    }
}
