package com.nozimjon.finalproject.dao;

import com.nozimjon.finalproject.builder.BookBuilder;
import com.nozimjon.finalproject.entity.Book;
import com.nozimjon.finalproject.util.constant.query.BookQuery;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

/**
 * The BookDAO class provides access to the Book database table and implements the IDAO interface for Book entities.
 */
public class BookDAO extends AbstractDAO<Book> {

    /**
     * Constructs a new BookDAO with the given database connection.
     *
     * @param connection the database connection
     */
    public BookDAO(Connection connection) {
        super(connection);
    }

    /**
     * Constructs a new BookDAO.
     */
    public BookDAO() {
        // Default constructor
    }

    @Override
    public Optional<Book> getById(long id) throws DAOException {
        return executeSingleResponseQuery(BookQuery.SELECT_BOOK_BY_ID, new BookBuilder(), String.valueOf(id));
    }

    @Override
    public List<Book> getAll() throws DAOException {
        return executeQuery(BookQuery.SELECT_ALL_BOOKS, new BookBuilder());
    }

    @Override
    public void save(Book book) throws DAOException {
        DataValidator.validateNotNull(book, "Not allowed for a null item in save at BookDao class");

        String[] bookInfo = {book.getName(), String.valueOf(book.getQuantity())};
        executeUpdate(BookQuery.INSERT_BOOK, bookInfo);
    }

    @Override
    public void removeById(long id) throws DAOException {
        executeUpdate(BookQuery.DELETE_BOOK, String.valueOf(id));
    }

    @Override
    public void update(Book book) throws DAOException {
        DataValidator.validateNotNull(book, "Not allowed for a null item in save at BookDao class");

        String[] bookInfo = {book.getName(), String.valueOf(book.getQuantity()), String.valueOf(book.getId())};
        executeUpdate(BookQuery.UPDATE_BOOK, bookInfo);
    }

    /**
     * Updates the quantity of a book in the database.
     *
     * @param bookId   the ID of the book to update
     * @param quantity the new quantity value
     * @throws DAOException if an error occurs while updating the quantity
     */
    public void updateQuantity(Long bookId, int quantity) throws DAOException {
        executeUpdate(BookQuery.UPDATE_BOOK_QUANTITY, String.valueOf(quantity), String.valueOf(bookId));
    }

    /**
     * Finds books by their name in the database.
     *
     * @param name the name of the book to search for
     * @return a list of books matching the name
     * @throws DAOException if an error occurs while executing the query
     */
    public List<Book> findByName(String name) throws DAOException {
        return executeQuery(BookQuery.SELECT_BOOK_BY_TITLE, new BookBuilder(), name);
    }

    // Sorting

    /**
     * Returns a list of books sorted by name.
     *
     * @return a list of books sorted by name
     * @throws DAOException if an error occurs while executing the query
     */
    public List<Book> sortBooksByName() throws DAOException {
        return executeQuery(BookQuery.SELECT_ALL_BOOKS_SORTED_BY_TITLE, new BookBuilder());
    }

    /**
     * Returns a list of books sorted by quantity.
     *
     * @return a list of books sorted by quantity
     * @throws DAOException if an error occurs while executing the query
     */
    public List<Book> sortBooksByQuantity() throws DAOException {
        return executeQuery(BookQuery.SELECT_ALL_BOOKS_SORTED_BY_QUANTITY, new BookBuilder());
    }
}
