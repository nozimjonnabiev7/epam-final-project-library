package com.nozimjon.finalproject.dao;

import java.sql.Connection;

/**
 * Factory class for creating instances of DAO classes.
 */
public class DAOFactory {

    private final Connection connection;

    /**
     * Constructs a new DAOFactory with the given database connection.
     *
     * @param connection the database connection
     */
    public DAOFactory(Connection connection) {
        this.connection = connection;
    }

    /**
     * Creates and returns a new instance of the UserDAO.
     *
     * @return a new UserDAO instance
     */
    public UserDAO getUserDao() {
        return new UserDAO(connection);
    }

    /**
     * Creates and returns a new instance of the BookDAO.
     *
     * @return a new BookDAO instance
     */
    public BookDAO getBookDao() {
        return new BookDAO(connection);
    }

    /**
     * Creates and returns a new instance of the OrderDAO.
     *
     * @return a new OrderDAO instance
     */
    public OrderDAO getOrderDao() {
        return new OrderDAO(connection);
    }
}
