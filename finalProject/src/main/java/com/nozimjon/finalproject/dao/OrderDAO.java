package com.nozimjon.finalproject.dao;

import com.nozimjon.finalproject.builder.AdministrationOrderBuilder;
import com.nozimjon.finalproject.builder.OrderBuilder;
import com.nozimjon.finalproject.builder.UserOrderBuilder;
import com.nozimjon.finalproject.entity.Order;
import com.nozimjon.finalproject.dto.AdministrationOrderDisplay;
import com.nozimjon.finalproject.dto.UserOrderDisplay;
import com.nozimjon.finalproject.util.constant.query.OrderQuery;
import com.nozimjon.finalproject.util.validator.DataValidator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Data Access Object (DAO) for handling database operations related to orders.
 */
public class OrderDAO extends AbstractDAO<Order> {

    /**
     * Constructs a new OrderDAO with the given database connection.
     *
     * @param connection the database connection
     */
    public OrderDAO(Connection connection) {
        super(connection);
    }

    /**
     * Default constructor.
     */
    public OrderDAO() {

    }

    @Override
    public Optional<Order> getById(long id) throws DAOException {
        return executeSingleResponseQuery(OrderQuery.SELECT_ORDER_BY_ID, new OrderBuilder(), String.valueOf(id));
    }

    @Override
    public List<Order> getAll() throws DAOException {
        return executeQuery(OrderQuery.SELECT_ALL_ORDERS, new OrderBuilder());
    }

    @Override
    public void save(Order item) throws DAOException {
        DataValidator.validateNotNull(item, "Not allow for a null item in save at OrderDao class");

        String[] orderInfo = {String.valueOf(item.getBookId()), String.valueOf(item.getUserId()), String.valueOf(item.getOrderDate()),
                String.valueOf(item.getReturningDate()), item.getReadingPlace().name()};
        executeUpdate(OrderQuery.INSERT_ORDER, orderInfo);
    }

    @Override
    public void removeById(long id) throws DAOException {
        executeUpdate(OrderQuery.DELETE_ORDER, String.valueOf(id));
    }

    @Override
    public void update(Order item) throws DAOException {
        DataValidator.validateNotNull(item, "Not allow for a null item in update at OrderDao class");

        String isReturned = item.isBookReturned() ? "1" : "0";
        String[] orderInfo = {String.valueOf(item.getBookId()), String.valueOf(item.getUserId()), String.valueOf(item.getOrderDate()),
                String.valueOf(item.getReturningDate()), item.getReadingPlace().name(), isReturned, String.valueOf(item.getOrderId())};

        executeUpdate(OrderQuery.UPDATE_ORDER_DATA, orderInfo);
    }

    /**
     * Retrieves a list of orders belonging to a specific user by their user ID.
     *
     * @param userId the ID of the user
     * @return a list of orders belonging to the user
     * @throws DAOException if there's an error executing the database query
     */
    public List<Order> findOrderByUserId(long userId) throws DAOException {
        return executeQuery(OrderQuery.SELECT_ORDER_BY_USER_ID, new OrderBuilder(), String.valueOf(userId));
    }

    /**
     * Retrieves an optional order based on the book ID.
     *
     * @param bookId the ID of the book
     * @return an optional order based on the book ID
     * @throws DAOException if there's an error executing the database query
     */
    public Optional<Order> findOrderByBookId(Long bookId) throws DAOException {
        return executeSingleResponseQuery(OrderQuery.SELECT_ORDER_BY_BOOK_ID, new OrderBuilder(), String.valueOf(bookId));
    }

    /**
     * Retrieves a list of orders for administration review.
     *
     * @return a list of orders for administration review
     * @throws DAOException if there's an error executing the database query
     */
    public List<AdministrationOrderDisplay> administrationAllOrder() throws DAOException {
        List<AdministrationOrderDisplay> orderDisplayList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(OrderQuery.SELECT_ORDER_FOR_REVIEW);

            prepareStatement(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                AdministrationOrderBuilder builder = new AdministrationOrderBuilder();
                AdministrationOrderDisplay orderDisplay = builder.build(resultSet);
                orderDisplayList.add(orderDisplay);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return orderDisplayList;
    }

    /**
     * Retrieves a list of orders for a user.
     *
     * @return a list of orders for a user
     * @throws DAOException if there's an error executing the database query
     */
    public List<UserOrderDisplay> userOrders() throws DAOException {
        List<UserOrderDisplay> orderDisplayList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(OrderQuery.SELECT_ORDER_FOR_USER);

            prepareStatement(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserOrderBuilder builder = new UserOrderBuilder();
                UserOrderDisplay orderDisplay = builder.build(resultSet);
                orderDisplayList.add(orderDisplay);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return orderDisplayList;
    }
}
