package com.nozimjon.finalproject.dao;

/**
 * Custom exception class for representing exceptions that occur in the DAO (Data Access Object) layer.
 */
public class DAOException extends Exception {

    /**
     * Constructs a new DAOException with no detail message.
     */
    public DAOException() {
        super();
    }

    /**
     * Constructs a new DAOException with the specified detail message.
     *
     * @param message the detail message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * Constructs a new DAOException with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause of the exception
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new DAOException with the specified cause.
     *
     * @param cause the cause of the exception
     */
    public DAOException(Throwable cause) {
        super(cause);
    }
}
