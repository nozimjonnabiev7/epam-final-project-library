package com.nozimjon.finalproject.util.validator;

public class DataValidator {

    private DataValidator() {
        // Private constructor to prevent instantiation; this class should be used statically.
    }

    public static void validateNonNegative(double number, String message) {
        if (number < 0) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEmptyString(String string, String message) {
        if (string == null || string.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }
}
