package com.nozimjon.finalproject.util;

import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.util.validator.DataValidator;
import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
public class EnumService {

    public static String getString(Enum[] values, String string) {
        DataValidator.validateNotNull(values, "Not allow for null enum values in EnumService");
        DataValidator.validateNotNull(string, "Not allow for null or empty string in EnumService");

        String findingValue = null;
        for (Enum value : values) {
            if (string.toUpperCase().equalsIgnoreCase(value.name())) {
                findingValue = value.name();
            }
        }
        return findingValue;
    }

    public static Roles getRole(String role) {
        DataValidator.validateNotEmptyString(role, "Not allow for null or empty string in EnumService");

        Roles findingValue = null;
        for (Roles value : Roles.values()) {
            if (role.trim().equalsIgnoreCase(value.name())) {
                findingValue = value;
            }
        }
        return findingValue;
    }

    public static ReadingPlace getReadingPlace(String readingPlace) {
        DataValidator.validateNotEmptyString(readingPlace, "Not allow for null or empty string in EnumService");
        ReadingPlace findingValue = null;
        for (ReadingPlace value : ReadingPlace.values()) {
            if (readingPlace.trim().equalsIgnoreCase(value.name())) {
                findingValue = value;
            }
        }
        return findingValue;
    }
}
