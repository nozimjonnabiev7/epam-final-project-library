package com.nozimjon.finalproject.util;

import com.nozimjon.finalproject.util.validator.DataValidator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Encryption {

    private static final String ALGORITHM = "MD5";

    private MD5Encryption() {
    }

    public static String encrypt(String toBeEncrypted) {
        DataValidator.validateNotEmptyString(toBeEncrypted, "Not null value is allowed");
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(ALGORITHM);
            byte[] bytes = toBeEncrypted.getBytes();
            byte[] digested = messageDigest.digest(bytes);

            for (byte b : digested) {
                sb.append(String.format("%02x", b & 0xff));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
