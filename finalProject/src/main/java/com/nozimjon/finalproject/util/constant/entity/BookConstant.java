package com.nozimjon.finalproject.util.constant.entity;

public class BookConstant {

    public final static String BOOK_ATTRIBUTE = "book";
    public final static String BOOK_LIST = "bookList";
    public final static String EDIT_BOOK = "editBook";
    public final static String BOOK_NOT_EXIST = "bookNotExist";


    public final static String BOOK_ID = "book_id";
    public final static String BOOK_NAME = "book_title";
    public final static String BOOK_QUANTITY = "quantity";
}
