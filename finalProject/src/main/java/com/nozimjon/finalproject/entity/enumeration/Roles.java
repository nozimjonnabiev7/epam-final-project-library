package com.nozimjon.finalproject.entity.enumeration;

public enum Roles {
    ADMIN,
    LIBRARIAN,
    READER
}
