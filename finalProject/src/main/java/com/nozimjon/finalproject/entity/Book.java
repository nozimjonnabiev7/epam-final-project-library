package com.nozimjon.finalproject.entity;

import com.nozimjon.finalproject.util.validator.DataValidator;

import java.util.Objects;

public class Book {

    private long id;
    private String name;
    private int quantity;

    public Book(long id, String name, int quantity) {
        DataValidator.validateNotNull(name, "Book name cannot be null!");
        DataValidator.validateNonNegative(quantity, "Book quantity cannot be negative!");

        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public Book(String name, int quantity) {
        DataValidator.validateNotNull(name, "Book name cannot be null!");
        DataValidator.validateNonNegative(quantity, "Book quantity cannot be negative!");

        this.name = name;
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        DataValidator.validateNotNull(name, "Book name cannot be null!");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        DataValidator.validateNonNegative(quantity, "Book quantity cannot be negative!");
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }
        Book book = (Book) o;
        return quantity == book.quantity &&
                id == book.id &&
                Objects.equals(name, book.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantity);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
