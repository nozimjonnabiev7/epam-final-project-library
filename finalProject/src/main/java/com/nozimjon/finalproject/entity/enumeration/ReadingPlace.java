package com.nozimjon.finalproject.entity.enumeration;

public enum ReadingPlace {
    HALL,
    HOME
}
