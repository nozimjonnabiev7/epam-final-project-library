package com.nozimjon.finalproject.entity.enumeration;

public enum BookReturnStatus {
    RETURNED("Returned"),
    NOT_RETURNED("Not Returned");

    private final String status;

    BookReturnStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return status;
    }
}
