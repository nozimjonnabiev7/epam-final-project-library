package com.nozimjon.finalproject.entity;

import java.util.Objects;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import com.nozimjon.finalproject.util.validator.DataValidator;

public class User {

    private long id;
    private String name;
    private String lastName;
    private String email;
    private String login;
    private String password;
    private Roles role;
    private boolean blocked;

    public User() {
    }

    public User(String name, String LastName, String email, String login, String password, Roles role) {
        setName(name);
        setLastName(LastName);
        setEmail(email);
        setLogin(login);
        setPassword(password);
        setRole(role);
    }

    public User(String name, String LastName, String email, String login, String password) {
        setName(name);
        setLastName(LastName);
        setEmail(email);
        setLogin(login);
        setPassword(password);
    }

    public User(long id, String name, String lastName, String email, String login, String password, Roles role, boolean blocked) {
        setId(id);
        setName(name);
        setLastName(lastName);
        setEmail(email);
        setLogin(login);
        setPassword(password);
        setRole(role);
        setBlocked(blocked);

    }

    public User(long id, String name, String lastName, String email, String login, String password, boolean blocked) {
        setId(id);
        setName(name);
        setLastName(lastName);
        setEmail(email);
        setLogin(login);
        setPassword(password);
        setBlocked(blocked);

    }

    public User(long id, String name, String lastName, String email, String login,Roles role, boolean blocked) {
        setId(id);
        setName(name);
        setLastName(lastName);
        setEmail(email);
        setLogin(login);
        setRole(role);
        setBlocked(blocked);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        DataValidator.validateNonNegative(id, "User ID cannot be negative in User class");
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        DataValidator.validateNotEmptyString(name, "User name cannot be null or empty in User class");
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        DataValidator.validateNotNull(lastName, "User last name cannot be null or empty in User class");
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        DataValidator.validateNotEmptyString(email, "User email cannot be null or empty in User class");
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        DataValidator.validateNotEmptyString(login, "User login cannot be null or empty in User class");
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        DataValidator.validateNotEmptyString(password, "User password cannot be null or empty in User class");
        this.password = password;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        DataValidator.validateNotNull(role, "User role cannot be null in User class");
        this.role = role;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                blocked == user.blocked &&
                Objects.equals(name, user.name) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, email, login, password, role, blocked);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", blocked=" + blocked +
                '}';
    }
}
