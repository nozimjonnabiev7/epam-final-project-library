<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="messages"/>

<html>

<head>
    <meta charset="utf-8">
    <meta name="description" content="Library Contact">
    <meta name="author" content="Nozimjon Nabiev">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="label.title.library" /> - <fmt:message key="nav.contact" /></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/contact.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>

<body>

<jsp:include page="/jsp/commoncode/navigation.jsp"/>
<jsp:include page="/jsp/commoncode/scrollTop.jsp"/>

<header>
    <h1 class="library-heading">Elite Minds Library</h1>
    <img class="library-image" src="${pageContext.request.contextPath}/img/contact_banner.png" alt="Contact Banner">
</header>

<section class="main-section">
    <div class="contact-info">
        <h2><fmt:message key="nav.contact" /></h2>
        <p class="info"><fmt:message key="label.contact.info" /></p>
        <div class="contact-details">
            <div class="contact-item">
                <i class="fas fa-map-marker-alt"></i>
                <p><strong><fmt:message key="label.address" />:</strong><br>123 Mustaqillik St, Tashkent</p>
            </div>
            <div class="contact-item">
                <i class="fas fa-envelope"></i>
                <p><strong><fmt:message key="label.email" />:</strong><br>elitemindslibrary@mail.ru</p>
            </div>
            <div class="contact-item">
                <i class="fas fa-phone"></i>
                <p><strong><fmt:message key="label.phone" />:</strong><br>+998 75 123-45-67</p>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/jsp/commoncode/footer.jsp"/>
</body>

</html>
