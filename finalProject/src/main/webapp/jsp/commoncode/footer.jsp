<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}" />
<fmt:setBundle basename="messages" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<c:choose>
    <%-- if the user is not in the system the footer will be as below--%>
    <c:when test="${sessionScope.user == null}">
        <footer class="footer">
            <div class="footLinks">
                <div class="footMenu">
                    <ul>
                        <li><a href="index.jsp"><fmt:message key="nav.home" /> </a></li>
                        <li><a href="login.jsp"><fmt:message key="label.login" /> </a></li>
                        <li><a href="news.jsp"><fmt:message key="nav.news" /> </a></li>
                        <li><a href="contact.jsp"><fmt:message key="nav.contact" /> </a></li>
                        <li><a href="#">About</a></li>
                    </ul>
                </div>
                <div class="socialMedia">
                    <ul>
                        <li><a href="https://www.facebook.com/#" target="_blank"><i class="fab fa-facebook"></i> <span>Facebook</span></a></li>
                        <li><a href="https://twitter.com/#" target="_blank"><i class="fab fa-twitter"></i> <span>Twitter</span></a></li>
                        <li><a href="https://www.linkedin.com/#" target="_blank"><i class="fab fa-linkedin"></i> <span>LinkedIn</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <p>&#169; Copyright: Elite Minds Library 2023</p>
                <p> <fmt:message key="message.dev.des.by"/> <a href="https://github.com/NozimjonNabiev">Nozimjon Nabiev</a></p>
                <br/>
            </div>
        </footer>
    </c:when>
    <%-- otherwise if the user in the system the footer will be as his own page footer--%>
    <c:otherwise>
        <footer class="profileFooter">
            <p><fmt:message key="message.for.help"/> <a href="contact.jsp"><fmt:message key="label.contact.us"/> </a></p>
            <p>
            <p>&#169; Copyright: Elite Minds Library 2023</p>
            <p> <fmt:message key="message.dev.des.by"/> <a href="https://github.com/NozimjonNabiev">Nozimjon Nabiev</a></p>
            <br/>
        </footer>
    </c:otherwise>
</c:choose>
