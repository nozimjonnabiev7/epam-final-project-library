const BLOCK = "block";
const NONE = "none";

const SEARCH_OPTION_ID = "select-option";
const SEARCH_FILED_ID = "search-field";
const READING_PLACE_ID = "reading-place";

const FIELD_TYPE = {
    EMAIL: "email",
    DATE: "date",
    TEXT: "text",
};

const SEARCH_FIELD_TYPE = {
    EMAIL: "email",
    ORDER_DATE: "orderDate",
    RETURNING_DATE: "returningDate",
    READING_PLACE: "readingPlace",
};

function changeSearchFieldType() {
    // Get the selected search option and relevant elements
    let selectOption = document.getElementById(SEARCH_OPTION_ID).value;
    let searchField = document.getElementById(SEARCH_FILED_ID);
    let readingPlaceField = document.getElementById(READING_PLACE_ID);

    // Set the default field type to text
    let fieldType = FIELD_TYPE.TEXT;
    let displayStyle = BLOCK;
    let isFieldDisabled = false;

    // Determine the field type and other properties based on the selected option
    switch (selectOption) {
        case SEARCH_FIELD_TYPE.EMAIL:
            fieldType = FIELD_TYPE.EMAIL;
            break;
        case SEARCH_FIELD_TYPE.ORDER_DATE:
        case SEARCH_FIELD_TYPE.RETURNING_DATE:
            fieldType = FIELD_TYPE.DATE;
            break;
        case SEARCH_FIELD_TYPE.READING_PLACE:
            displayStyle = NONE;
            isFieldDisabled = true;
            break;
    }

    // Update the search field properties accordingly
    searchField.setAttribute(TYPE, fieldType);
    searchField.disabled = isFieldDisabled;
    searchField.style.display = displayStyle;
    readingPlaceField.style.display = displayStyle === BLOCK ? NONE : BLOCK;
}

