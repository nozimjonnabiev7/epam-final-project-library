function userFormValidation() {
    let userName = document.getElementById("user-name");
    let userLastName = document.getElementById("user-last-name");
    let email = document.getElementById("email");
    let login = document.getElementById("user-login");
    let password = document.getElementById("user-password");

    let errors = [];

    if (!nameRegex.test(userName.value.toLowerCase())) {
        errors.push("Invalid first name. It should contain only letters, numbers, and spaces (up to 30 characters).");
        userName.style.border = "1px solid red";
    } else {
        userName.style.border = "1px solid #ccc"; // Reset border style on success
    }

    if (!nameRegex.test(userLastName.value.toLowerCase())) {
        errors.push("Invalid last name. It should contain only letters, numbers, and spaces (up to 30 characters).");
        userLastName.style.border = "1px solid red";
    } else {
        userLastName.style.border = "1px solid #ccc"; // Reset border style on success
    }

    if (!emailRegex.test(email.value.toLowerCase())) {
        errors.push("Invalid email address.");
        email.style.border = "1px solid red";
    } else {
        email.style.border = "1px solid #ccc"; // Reset border style on success
    }

    if (!loginRegex.test(login.value.toLowerCase())) {
        errors.push("Invalid login. It should contain only letters, numbers, and underscores (up to 10 characters).");
        login.style.border = "1px solid red";
    } else {
        login.style.border = "1px solid #ccc"; // Reset border style on success
    }

    if (!passwordRegex.test(password.value.toLowerCase())) {
        errors.push("Invalid password. It should contain only letters, numbers, and the characters '@', '*', or '#' (4 to 10 characters).");
        password.style.border = "1px solid red";
    } else {
        password.style.border = "1px solid #ccc"; // Reset border style on success
    }

    if (errors.length > 0) {
        // Display the error messages
        alert(errors.join("\n"));
        return false;
    }

    // Validation passed
    return true;
}
