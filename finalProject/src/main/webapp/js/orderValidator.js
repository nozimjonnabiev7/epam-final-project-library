window.onload = function () {
    const today = new Date().toISOString().substr(0, 10);
    const orderDateInput = document.getElementById("order-date");
    const orderDate = orderDateInput.value;

    // Check if the order date is valid, and if not, set it to today's date
    if (!Date.parse(orderDate)) {
        orderDateInput.value = today;
    }
};

function validateReturningDate() {
    const orderDateInput = document.getElementById("order-date");
    const returningDateInput = document.getElementById("returning-date");
    const returningDateMessage = document.getElementById("returning-date-message");

    const orderDate = orderDateInput.value;
    const returningDate = returningDateInput.value;

    if (returningDate < orderDate) {
        // Show an error message and adjust the returning date if it's before the order date
        returningDateInput.style.border = "1px solid red";
        returningDateInput.value = orderDate;
        returningDateMessage.style.display = "block";
        returningDateMessage.style.color = "red";
        return false;
    }

    // Validation passed
    return true;
}

function orderFormValidation() {
    if (!validateReturningDate()) {
        return false;
    }

    const returnedMessage = document.getElementById("returned-message").value;
    const returnedValue = document.forms["administration-update-order"]["book_returned"].value;

    // If the book is marked as returned, show a confirmation message
    if (returnedValue === "true") {
        return confirm(returnedMessage);
    }

    // Validation passed
    return true;
}
