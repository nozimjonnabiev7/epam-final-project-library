<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="messages"/>


<!DOCTYPE html>
<html lang="${sessionScope.language}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Library">
    <meta name="author" content="Nozimjon Nabiev">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="label.title.library" /></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index.css">
</head>

<body>

<jsp:include page="/jsp/commoncode/navigation.jsp"/>
<jsp:include page="/jsp/commoncode/scrollTop.jsp"/>

<header>
    <h1 class="library-heading">Elite Minds Library</h1>
    <img class="library-image" src="${pageContext.request.contextPath}/img/homepage.png" alt="Library Logo">
</header>

<section class="main-section">
    <div class="library-text">
        <h2>Latest News</h2>
        <p>Welcome to Elite Minds Library! Discover our captivating collection of mystery novels, filled with suspense, twists, and unforgettable characters.</p>

        <p>Immerse yourself in historical fiction with books set in different eras and locations, bringing history to life through rich narratives.</p>
    </div>
</section>

<section class="second-section">
    <div class="column">
        <h2>Library Services</h2>
        <p>Enhance your reading experience with our wide range of services. Access our vast digital collection for online borrowing and reserve your favorite titles for in-person pickup.</p>
    </div>
    <div class="column">
        <h2>Upcoming Events</h2>
        <p>Join our upcoming author meet-and-greet event. Meet your favorite authors, get signed copies of their latest books, and engage in exciting book discussions.</p>
    </div>
    <div class="column">
        <h2>Awards and Recognition</h2>
        <p>Elite Minds Library has been recognized for promoting literacy and education. We are honored to receive the "Library of the Year" award for fostering a love for reading.</p>
    </div>
</section>

<jsp:include page="/jsp/commoncode/footer.jsp"/>
</body>

</html>
