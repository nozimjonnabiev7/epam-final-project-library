<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.language}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Library">
    <meta name="author" content="Nozimjon Nabiev">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><fmt:message key="label.title.library" /></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/news.css">
</head>

<body>

<jsp:include page="/jsp/commoncode/navigation.jsp"/>
<jsp:include page="/jsp/commoncode/scrollTop.jsp"/>

<header>
    <h1 class="library-heading">Elite Minds Library</h1>
    <img class="library-image" src="${pageContext.request.contextPath}/img/homepage.png" alt="Library Logo">
</header>

<section class="main-section">
    <div class="library-text">
        <h2>Latest News</h2>
        <div class="article">
            <h3>Grand Opening of Elite Minds Library</h3>
            <p class="date">15th January 2023</p>
            <p>We are excited to announce the grand opening of Elite Minds Library. Come and explore our vast collection of books and resources!</p>
        </div>
        <div class="article">
            <h3>Author Reading Event</h3>
            <p class="date">25th February 2023</p>
            <p>Join us for a special author reading event by acclaimed author John Smith. He will be discussing his latest book and answering questions from the audience.</p>
        </div>
        <div class="article">
            <h3>Summer Reading Challenge</h3>
            <p class="date">1st June 2023</p>
            <p>Participate in our exciting summer reading challenge! Read as many books as you can during the summer, and win exciting prizes.</p>
        </div>
    </div>
</section>

<jsp:include page="/jsp/commoncode/footer.jsp"/>
</body>

</html>
