package com.nozimjon.finalproject.util;

import org.junit.Assert;
import org.junit.Test;

public class MD5EncryptionTest {

    private static final String TEXT = "test";
    private static final String EMPTY_TEXT = "";
    private static final String NULL_TEXT = null;

    @Test
    public void testEncryptValidText() {
        String actual = MD5Encryption.encrypt(TEXT);
        String expected = MD5Encryption.encrypt(TEXT);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEncryptNullText() {
        MD5Encryption.encrypt(NULL_TEXT);
    }

    @Test
    public void testEncryptLongText() {
        String longText = generateLongText();
        String actual = MD5Encryption.encrypt(longText);
        String expected = MD5Encryption.encrypt(longText);
        Assert.assertEquals(expected, actual);
    }

    private String generateLongText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 10000; i++) {
            stringBuilder.append("abcdefghijklmnopqrstuvwxyz");
        }
        return stringBuilder.toString();
    }
}
