package com.nozimjon.finalproject.util;

import com.nozimjon.finalproject.util.validator.DataRegex;
import org.junit.Assert;
import org.junit.Test;

public class DataRegexTest {

    @Test
    public void testEmailRegex_ValidEmail() {
        Assert.assertTrue("test@example.com".matches(DataRegex.E_MAIL));
        Assert.assertTrue("user123@test.co.uk".matches(DataRegex.E_MAIL));
        Assert.assertTrue("info@my-domain.com".matches(DataRegex.E_MAIL));
    }

    @Test
    public void testEmailRegex_InvalidEmail() {
        Assert.assertFalse("test.example.com".matches(DataRegex.E_MAIL));
        Assert.assertFalse("user@domain".matches(DataRegex.E_MAIL));
        Assert.assertFalse("user@.com".matches(DataRegex.E_MAIL));
    }

    @Test
    public void testLoginRegex_ValidLogin() {
        Assert.assertTrue("username".matches(DataRegex.LOGIN));
        Assert.assertTrue("user123".matches(DataRegex.LOGIN));
        Assert.assertTrue("user_name".matches(DataRegex.LOGIN));
    }

    @Test
    public void testAnyNumberRegex_ValidNumbers() {
        Assert.assertTrue("123".matches(DataRegex.ANY_NUMBER));
        Assert.assertTrue("0".matches(DataRegex.ANY_NUMBER));
        Assert.assertTrue("99999".matches(DataRegex.ANY_NUMBER));
    }

    @Test
    public void testAnyNumberRegex_InvalidNumbers() {
        Assert.assertFalse("1.23".matches(DataRegex.ANY_NUMBER));
        Assert.assertFalse("1a23".matches(DataRegex.ANY_NUMBER));
        Assert.assertFalse("abc".matches(DataRegex.ANY_NUMBER));
    }

    @Test
    public void testPositiveNumberOnlyExcludeZeroRegex_ValidNumbers() {
        Assert.assertTrue("1".matches(DataRegex.POSITIVE_NUMBER_ONLY_EXCLUDE_ZERO));
        Assert.assertTrue("99999".matches(DataRegex.POSITIVE_NUMBER_ONLY_EXCLUDE_ZERO));
    }

    @Test
    public void testPositiveNumberOnlyExcludeZeroRegex_InvalidNumbers() {
        Assert.assertFalse("0".matches(DataRegex.POSITIVE_NUMBER_ONLY_EXCLUDE_ZERO));
        Assert.assertFalse("-123".matches(DataRegex.POSITIVE_NUMBER_ONLY_EXCLUDE_ZERO));
        Assert.assertFalse("abc".matches(DataRegex.POSITIVE_NUMBER_ONLY_EXCLUDE_ZERO));
    }

    @Test
    public void testPasswordRegex_ValidPasswords() {
        Assert.assertTrue("Abc123@#".matches(DataRegex.PASSWORD));
        Assert.assertTrue("Pass@123".matches(DataRegex.PASSWORD));
    }

    @Test
    public void testPasswordRegex_InvalidPasswords() {
        Assert.assertFalse("1234".matches(DataRegex.PASSWORD));
        Assert.assertFalse("abc".matches(DataRegex.PASSWORD));
        Assert.assertFalse("password".matches(DataRegex.PASSWORD));
    }

    @Test
    public void testLimitedWordRegex_ValidWords() {
        Assert.assertTrue("Hello World".matches(DataRegex.LIMITED_WORD));
        Assert.assertTrue("This is a test".matches(DataRegex.LIMITED_WORD));
    }

}
