package com.nozimjon.finalproject.util;

import com.nozimjon.finalproject.entity.enumeration.ReadingPlace;
import com.nozimjon.finalproject.entity.enumeration.Roles;
import org.junit.Assert;
import org.junit.Test;

public class EnumServiceTest {

    @Test
    public void testGetStringValidValue() {
        ReadingPlace[] values = ReadingPlace.values();
        String stringValue = "HOME";
        String expected = "HOME";
        String actual = EnumService.getString(values, stringValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetStringInvalidValue() {
        ReadingPlace[] values = ReadingPlace.values();
        String stringValue = "INVALID";
        String expected = null;
        String actual = EnumService.getString(values, stringValue);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStringNullValues() {
        ReadingPlace[] values = null;
        String stringValue = "HOME";
        EnumService.getString(values, stringValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStringNullString() {
        ReadingPlace[] values = ReadingPlace.values();
        String stringValue = null;
        EnumService.getString(values, stringValue);
    }

    @Test
    public void testGetRoleValidRole() {
        String role = "ADMIN";
        Roles expected = Roles.ADMIN;
        Roles actual = EnumService.getRole(role);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetRoleInvalidRole() {
        String role = "INVALID";
        Roles expected = null;
        Roles actual = EnumService.getRole(role);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRoleNullString() {
        String role = null;
        EnumService.getRole(role);
    }

    @Test
    public void testGetReadingPlaceValidPlace() {
        String readingPlace = "HOME";
        ReadingPlace expected = ReadingPlace.HOME;
        ReadingPlace actual = EnumService.getReadingPlace(readingPlace);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetReadingPlaceInvalidPlace() {
        String readingPlace = "INVALID";
        ReadingPlace expected = null;
        ReadingPlace actual = EnumService.getReadingPlace(readingPlace);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetReadingPlaceNullString() {
        String readingPlace = null;
        EnumService.getReadingPlace(readingPlace);
    }
}
